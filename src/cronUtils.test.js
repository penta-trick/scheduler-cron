var expect = require('chai').expect;

var cronUtils = require('./cronUtils');
var dateUtils = require('./dateUtils');
var Cron = require('./Cron');

describe('cron utils', function() {
    describe('getInterval(cron, date)', function() {
        it('* * * * * *', function() {
            var cron = new Cron('* * * * * *');
            expect(cronUtils.getInterval(cron)).to.be.equal(dateUtils.timePeriods.minute);
        });

        it('m * * * * *', function() {
            var cron = new Cron('5 * * * * *');
            expect(cronUtils.getInterval(cron)).to.be.equal(dateUtils.timePeriods.hour);
        });

        it('m h * * * *', function() {
            var cron = new Cron('5 15 * * * *');
            expect(cronUtils.getInterval(cron)).to.be.equal(dateUtils.timePeriods.day);
        });

        it('m h d * * *', function() {
            var cron = new Cron('5 15 13 * * *');
            var date = new Date(2016, 8, 6);
            var nextDate = new Date(2016, 9, 6);
            var intervalFunc = cronUtils.getInterval(cron, date);

            expect(intervalFunc()).to.be.equal(nextDate.getTime() - date.getTime());
        });

        it('m h d M * *', function() {
            var cron = new Cron('5 15 13 8 * *');
            var date = new Date(2016, 8, 6);
            var nextDate = new Date(2017, 8, 6);
            var intervalFunc = cronUtils.getInterval(cron, date);

            expect(intervalFunc()).to.be.equal(nextDate.getTime() - date.getTime());
        });

        it('m h * * w *', function() {
            var cron = new Cron('5 15 * * 3 *');
            expect(cronUtils.getInterval(cron)).to.be.equal(dateUtils.timePeriods.week);
        });

        it('m h d * w *', function() {
            var cron = new Cron('5 15 13 * 4 *');
            var date = new Date(2016, 8, 6);
            var nextDate = new Date(2016, 9, 6);
            var intervalFunc = cronUtils.getInterval(cron, date);

            expect(intervalFunc()).to.be.deep.equal(nextDate.getTime() - date.getTime());
        });
    });

    describe('getNextExecutionDate(cron, date)', function() {
        it('returns correct date for "m * * * * *" cron if date is in this hour', function() {
            var date = new Date(2016, 8, 3, 12, 3, 36);
            var cron = new Cron('16 * * * * *');

            expect(
                cronUtils.getNextExecutionDate(cron, date)
            ).to.be.deep.equal(new Date(2016, 8, 3, 12, 16));
        });

        it('returns correct date for "m * * * * *" cron if date is in the next hour', function() {
            var date = new Date(2016, 8, 3, 12, 29, 36);
            var cron = new Cron('16 * * * * *');

            expect(
                cronUtils.getNextExecutionDate(cron, date)
            ).to.be.deep.equal(new Date(2016, 8, 3, 13, 16));
        });

        it('returns correct date for "m h * * * *" cron if date is in this day', function() {
            var date = new Date(2016, 8, 3, 8, 3, 36);
            var cron = new Cron('8 10 * * * *');

            expect(
                cronUtils.getNextExecutionDate(cron, date)
            ).to.be.deep.equal(new Date(2016, 8, 3, 10, 8));
        });

        it('returns correct date for "m h * * * *" cron if date is in the next day', function() {
            var date = new Date(2016, 8, 3, 12, 29, 36);
            var cron = new Cron('8 10 * * * *');

            expect(
                cronUtils.getNextExecutionDate(cron, date)
            ).to.be.deep.equal(new Date(2016, 8, 4, 10, 8));
        });

        it('returns correct date for "m h d * * *" cron if date is in this month', function() {
            var date = new Date(2016, 8, 3, 8, 3, 36);
            var cron = new Cron('4 16 20 * * *');

            expect(
                cronUtils.getNextExecutionDate(cron, date)
            ).to.be.deep.equal(new Date(2016, 8, 20, 16, 4));
        });

        it('returns correct date for "m h d * * *" cron if date is in the next month', function() {
            var date = new Date(2016, 8, 25, 12, 29, 36);
            var cron = new Cron('4 16 20 * * *');

            expect(
                cronUtils.getNextExecutionDate(cron, date)
            ).to.be.deep.equal(new Date(2016, 9, 20, 16, 4));
        });

        it('returns correct date for "m h d M * *" cron if date is in this year', function() {
            var date = new Date(2016, 1, 3, 8, 3, 36);
            var cron = new Cron('4 16 20 3 * *');

            expect(
                cronUtils.getNextExecutionDate(cron, date)
            ).to.be.deep.equal(new Date(2016, 2, 20, 16, 4));
        });

        it('returns correct date for "m h d M * *" cron if date is in the next year', function() {
            var date = new Date(2016, 8, 25, 12, 29, 36);
            var cron = new Cron('4 16 20 3 * *');

            expect(
                cronUtils.getNextExecutionDate(cron, date)
            ).to.be.deep.equal(new Date(2017, 2, 20, 16, 4));
        });

        it('returns correct date for "m h d M * Y" cron', function() {
            var date = new Date(2016, 1, 3, 8, 3, 36);
            var cron = new Cron('4 16 20 3 * 2017');

            expect(
                cronUtils.getNextExecutionDate(cron, date)
            ).to.be.deep.equal(new Date(2017, 2, 20, 16, 4));
        });

        it('returns null for "m h d M * Y" cron if date has passed', function() {
            var date = new Date(2016, 8, 25, 12, 29, 36);
            var cron = new Cron('4 16 20 3 * 2015');

            expect(cronUtils.getNextExecutionDate(cron, date)).to.be.null;
        });

        it('returns correct date for "m h 29 2 * *" cron', function() {
            var cron = new Cron('15 12 29 2 * *');

            expect(
                cronUtils.getNextExecutionDate(cron, new Date(2016, 8, 6))
            ).to.be.deep.equal(
                new Date(2020, 1, 29, 12, 15)
            );
        });

        it('returns correct date for 0 hour cron', function() {
            var cron = new Cron('5 0 * * * *');

            expect(
                cronUtils.getNextExecutionDate(cron, new Date(2016, 10, 19))
            ).to.be.deep.equal(
                new Date(2016, 10, 19, 0, 5)
            );
        });

        it('returns correct date for "* * * * * * *" cron', function() {
            var cron = new Cron('* * * * * * *');

            expect(
                cronUtils.getNextExecutionDate(cron, new Date(2016, 10, 25, 23, 32, 21, 344))
            ).to.be.deep.equal(
                new Date(2016, 10, 25, 23, 32, 22)
            );
        });
    });

    describe('normalizeCron(cronString)', function() {
        it('returns normalized cron string if it contains minute step', function() {
            expect(
                cronUtils.normalizeCron('*/12 * * * * *')
            ).to.be.equal(
                '0 0 0,12,24,36,48 * * * * *'
            );
        });

        it('returns normalized cron string if it contains hour step', function() {
            expect(
                cronUtils.normalizeCron('* */10 * * * *')
            ).to.be.equal(
                '0 0 * 0,10,20 * * * *'
            );
        });

        it('returns normalized cron string if it contains day step', function() {
            expect(
                cronUtils.normalizeCron('3 * */8 * * *')
            ).to.be.equal(
                '0 0 3 * 0,8,16,24 * * *'
            );
        });

        it('returns normalized cron string if it contains month step', function() {
            expect(
                cronUtils.normalizeCron('* 0 * */9 * *')
            ).to.be.equal(
                '0 0 * 0 * 0,9 * *'
            );
        });

        it('returns normalized cron string if it contains weekday step', function() {
            expect(
                cronUtils.normalizeCron('3 * * * */2 *')
            ).to.be.equal(
                '0 0 3 * * * 0,2,4,6 *'
            );
        });

        it('returns normalized cron string if it contains minute step and range', function() {
            expect(
                cronUtils.normalizeCron('3-9/3 * * * * *')
            ).to.be.equal(
                '0 0 3,6,9 * * * * *'
            );
        });

        it('returns normalized cron string if it has extended format', function() {
            expect(cronUtils.normalizeCron('* 15')).to.be.equal('0 0 * 15 * * * *');
        });

        it('returns normalized cron string if it is empty', function() {
            expect(cronUtils.normalizeCron('')).to.be.equal('0 0 * * * * * *');
        });

        it('returns normalized cron string if second is specified', function() {
            expect(
                cronUtils.normalizeCron('* 3 * * * * *')
            ).to.be.equal(
                '0 * 3 * * * * *'
            );
        });

        it('returns the same string if millisecond is specified', function() {
            expect(
                cronUtils.normalizeCron('* 5 3 * * * * *')
            ).to.be.equal(
                '* 5 3 * * * * *'
            );
        });
    });
});
