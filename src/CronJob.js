var TimeExecutor = require('./TimeExecutor');
var IntervalJob = require('./IntervalJob');
var Cron = require('./Cron');
var cronUtils = require('./cronUtils');
var dateUtils = require('./dateUtils');

function createNestedCronJob(cron, executionCount, func) {
    var cronJob = new CronJob(cron, function() {
        executionCount--;

        if(executionCount <= 0) {
            cronJob.stop();
            return;
        }

        func();
    });

    return cronJob;
}

function createCronJobWithOneNesting(cron, func) {
    return new CronJob(cron.main, function() {
        createNestedCronJob(
            cron.nested,
            cron.getNestedExecutionCount(new Date()),
            func
        );

        func();
    });
}

function createCronJobWithTwoNestings(cron, func) {
    return new CronJob(cron.main, function() {
        var now = new Date();

        createNestedCronJob(
            cron.nested.nested,
            cron.nested.getNestedExecutionCount(now),
            func
        );

        createNestedCronJob(
            cron.nested.main,
            cron.getNestedExecutionCount(now),
            function() {
                createNestedCronJob(
                    cron.nested.nested,
                    cron.nested.getNestedExecutionCount(new Date()),
                    func
                );

                func();
            }
        );

        func();
    });
}

function createIntervalJob(interval, func, options, condition) {
    if(condition) {
        return new IntervalJob(interval, function() {
            condition() && func();
        }, options);
    } else {
        return new IntervalJob(interval, func, options);
    }
}

function getWeekdayCondition(weekday) {
    weekday %= dateUtils.timeSizes.daysInWeek;

    return function() {
        var now = new Date();
        return now.getDay() === weekday;
    };
}

function CronJob(cron, func, options) {
    options = options || {};

    var useTimeout = options.useTimeout;

    var intervalJob;
    var timeExecutor;
    var cronJob;
    var cronJobs;

    if (typeof cron === 'string') {
        cron = new Cron(cron);
    }

    if(cron.hasArray) {
        cronJobs = [];
        var cronArray = cron.array;

        for(var i = 0, n = cronArray.length; i < n; i++) {
            cronJobs.push(new CronJob(cronArray[i], func));
        }
    } else if(cron.hasNesting) {
        cronJob = cron.nested.hasNesting
            ? createCronJobWithTwoNestings(cron, func)
            : createCronJobWithOneNesting(cron, func);
    } else {
        var nextExecutionDate = cronUtils.getNextExecutionDate(cron, new Date());

        if (cron.cyclicField === undefined) {
            timeExecutor = new TimeExecutor(nextExecutionDate, func);
        } else {
            var condition = !(cron.weekday.asterisk || cron.day.asterisk)
                ? getWeekdayCondition(cron.weekday.value)
                : undefined;

            intervalJob = createIntervalJob(
                cronUtils.getInterval(cron, nextExecutionDate),
                func,
                {
                    startDate: nextExecutionDate,
                    useTimeout: useTimeout
                },
                condition
            );
        }
    }

    this.stop = function() {
        timeExecutor && timeExecutor.cancel();
        intervalJob && intervalJob.stop();
        cronJob && cronJob.stop();

        cronJobs && cronJobs.forEach(function(currentCronJob) {
            currentCronJob.stop();
        });
    };
}

module.exports = CronJob;
