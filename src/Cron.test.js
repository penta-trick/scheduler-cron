var expect = require('chai').expect;
var Cron = require('./Cron');

var MINUTES_IN_HOUR = 60;
var HOURS_IN_DAY = 24;
var MONTHS_IN_YEAR = 12;

function getMonthDaysCount(month, year) {
    if(month === 2) {
        return (year % 4 > 0) ? 28 : 29;
    }

    if(
        (month <= 7 && month % 2 === 1)
        || (month > 7 && month % 2 === 0)
    ) {
        return 31;
    }

    return 30;
}

function getYearDaysCount(year) {
    var isLeapYear = (year % 4 === 0 && year % 100 !== 0)
        || (year % 400 === 0);

    return isLeapYear ? 366 : 365;
}

describe('Cron', function() {
    describe('parsing', function() {
        it('contains correct keys', function() {
            var cron = new Cron('* * * * * *');

            expect(cron.fields).to.be.deep.equal([
                'millisecond',
                'second',
                'minute',
                'hour',
                'day',
                'month',
                'weekday',
                'year'
            ]);
        });

        it('has correct information about fields if value is asterisk', function() {
            var cron = new Cron('* * * * * * * *');

            expect(cron.second.asterisk).to.be.true;
            expect(cron.minute.asterisk).to.be.true;
            expect(cron.hour.asterisk).to.be.true;
            expect(cron.day.asterisk).to.be.true;
            expect(cron.month.asterisk).to.be.true;
            expect(cron.weekday.asterisk).to.be.true;
            expect(cron.year.asterisk).to.be.true;
        });

        it('has correct information about fields if value is number', function() {
            var cron = new Cron('231 15 53 12 27 6 5 2017');

            expect(cron.millisecond.asterisk).to.be.false;
            expect(cron.millisecond.value).to.be.equal(231);

            expect(cron.second.asterisk).to.be.false;
            expect(cron.second.value).to.be.equal(15);

            expect(cron.minute.asterisk).to.be.false;
            expect(cron.minute.value).to.be.equal(53);

            expect(cron.hour.asterisk).to.be.false;
            expect(cron.hour.value).to.be.equal(12);

            expect(cron.day.asterisk).to.be.false;
            expect(cron.day.value).to.be.equal(27);

            expect(cron.month.asterisk).to.be.false;
            expect(cron.month.value).to.be.equal(6);

            expect(cron.weekday.asterisk).to.be.false;
            expect(cron.weekday.value).to.be.equal(5);

            expect(cron.year.asterisk).to.be.false;
            expect(cron.year.value).to.be.equal(2017);
        });

        it('returns correct string if the toString() is called', function() {
            var cron = new Cron('615 13 4 20 16 3 1 2020');
            expect(cron.toString()).to.be.equal('615 13 4 20 16 3 1 2020');
        });

        it('creates correct cron if the object is used', function() {
            var cron = new Cron({
                millisecond: { asterisk: false, value: 899 },
                second: { asterisk: true },
                minute: { asterisk: false, value: 10 },
                hour: { asterisk: false, value: 6 },
                day: { asterisk: false, value: 3 },
                month: { asterisk: true },
                weekday: { asterisk: true },
                year: { asterisk: true }
            });

            expect(cron.toString()).to.be.equal('899 * 10 6 3 * * *');
        });
    });

    describe('nesting', function() {
        it('has correct hasNesting field if cron is simple', function() {
            var cron = new Cron('4 5 7 * * *');
            expect(cron.hasNesting).to.be.false;
        });

        it('has correct main and nested crons if cron should be splitted once', function() {
            var cron = new Cron('* * 7 2 * *');
            expect(cron.hasNesting).to.be.true;

            expect(cron.main).instanceOf(Cron);
            expect(cron.main.toString()).to.be.equal('0 0 0 0 7 2 * *');
            expect(cron.main.hasNesting).to.be.false;

            expect(cron.nested).instanceOf(Cron);
            expect(cron.nested.toString()).to.be.equal('0 0 * * * * * *');
            expect(cron.nested.hasNesting).to.be.false;
        });

        it('has correct main and nested crons if cron should be splitted twice', function() {
            var cron = new Cron('* 10 * 2 * *');
            expect(cron.hasNesting).to.be.true;

            expect(cron.main).instanceOf(Cron);
            expect(cron.main.toString()).to.be.equal('0 0 0 10 1 2 * *');
            expect(cron.main.hasNesting).to.be.false;

            expect(cron.nested).instanceOf(Cron);
            expect(cron.nested.toString()).to.be.equal('0 0 * 10 * * * *');
            expect(cron.nested.hasNesting).to.be.true;

            expect(cron.nested.main).instanceOf(Cron);
            expect(cron.nested.main.toString()).to.be.equal('0 0 0 10 * * * *');

            expect(cron.nested.nested).instanceOf(Cron);
            expect(cron.nested.nested.toString()).to.be.equal('0 0 * * * * * *');
        });

        it('has correct main and nested crons for "* * * * w *', function() {
            var cron = new Cron('* * * * 5 *');
            expect(cron.main.toString()).to.be.equal('0 0 0 0 * * 5 *');
            expect(cron.nested.toString()).to.be.equal('0 0 * * * * * *');
        });
    });

    describe('array', function() {
        it('has correct hasArray field if cron is simple', function() {
            var cron = new Cron('6 1 2 * * *');
            expect(cron.hasArray).to.be.false;
        });

        it('has correct array for single comma range', function() {
            var cron = new Cron('* * 7,10,12 * * *');
            expect(cron.hasArray).to.be.true;
            expect(cron.array).to.have.length(3);

            expect(cron.array[0].toString()).to.be.equal('0 0 * * 7 * * *');
            expect(cron.array[1].toString()).to.be.equal('0 0 * * 10 * * *');
            expect(cron.array[2].toString()).to.be.equal('0 0 * * 12 * * *');
        });

        it('has correct array for multiple comma ranges', function() {
            var cron = new Cron('3,4 6,7 1,10 * * *');
            expect(cron.hasArray).to.be.true;
            expect(cron.array).to.have.length(8);

            expect(cron.array[0].toString()).to.be.equal('0 0 3 6 1 * * *');
            expect(cron.array[1].toString()).to.be.equal('0 0 3 6 10 * * *');
            expect(cron.array[2].toString()).to.be.equal('0 0 3 7 1 * * *');
            expect(cron.array[3].toString()).to.be.equal('0 0 3 7 10 * * *');
            expect(cron.array[4].toString()).to.be.equal('0 0 4 6 1 * * *');
            expect(cron.array[5].toString()).to.be.equal('0 0 4 6 10 * * *');
            expect(cron.array[6].toString()).to.be.equal('0 0 4 7 1 * * *');
            expect(cron.array[7].toString()).to.be.equal('0 0 4 7 10 * * *');
        });

        it('has correct array for single hyphen range', function() {
            var cron = new Cron('* * 7-10 * * *');
            expect(cron.hasArray).to.be.true;
            expect(cron.array).to.have.length(4);

            expect(cron.array[0].toString()).to.be.equal('0 0 * * 7 * * *');
            expect(cron.array[1].toString()).to.be.equal('0 0 * * 8 * * *');
            expect(cron.array[2].toString()).to.be.equal('0 0 * * 9 * * *');
            expect(cron.array[3].toString()).to.be.equal('0 0 * * 10 * * *');
        });

        it('has correct array for multiple hyphen ranges', function() {
            var cron = new Cron('* 0-2 5-6 * * *');
            expect(cron.hasArray).to.be.true;
            expect(cron.array).to.have.length(6);

            expect(cron.array[0].toString()).to.be.equal('0 0 * 0 5 * * *');
            expect(cron.array[1].toString()).to.be.equal('0 0 * 0 6 * * *');
            expect(cron.array[2].toString()).to.be.equal('0 0 * 1 5 * * *');
            expect(cron.array[3].toString()).to.be.equal('0 0 * 1 6 * * *');
            expect(cron.array[4].toString()).to.be.equal('0 0 * 2 5 * * *');
            expect(cron.array[5].toString()).to.be.equal('0 0 * 2 6 * * *');
        });
    });

    describe('condition', function() {
        it('has correct hasCondition field if weekday is specified', function() {
            var cron = new Cron('1 2 3 4 5 *');
            expect(cron.hasCondition).to.be.true;
        });

        it('has correct hasCondition field if year is specified', function() {
            var cron = new Cron('1 2 3 4 * 2017');
            expect(cron.hasCondition).to.be.true;
        });

        it('has correct hasCondition field if weekday and year are not specified', function() {
            var cron = new Cron('1 2 3 4 * *');
            expect(cron.hasCondition).to.be.false;
        });

        it('isDateCorrect returns correct value if weekday is specified', function() {
            var cron = new Cron('1 2 3 4 5 *');
            var correctDate = new Date(2016, 8, 9);    // 09.09.2016 Fri
            var incorrectDate = new Date(2016, 8, 10); // 10.09.2016 Sat

            expect(cron.isDateCorrect(correctDate)).to.be.true;
            expect(cron.isDateCorrect(incorrectDate)).to.be.false;
        });

        it('isDateCorrect returns correct value if year is specified', function() {
            var cron = new Cron('1 2 3 4 * 2016');
            var correctDate = new Date(2016, 0, 1);
            var incorrectDate = new Date(2017, 0, 1);

            expect(cron.isDateCorrect(correctDate)).to.be.true;
            expect(cron.isDateCorrect(incorrectDate)).to.be.false;
        });
    });

    describe('cyclicField', function() {
        it('* * * * * *', function() {
            var cron = new Cron('* * * * * *');
            expect(cron.cyclicField).to.be.equal(cron.fieldIndices.minute);
        });

        it('m * * * * *', function() {
            var cron = new Cron('2 * * * * *');
            expect(cron.cyclicField).to.be.equal(cron.fieldIndices.hour);
        });

        it('m h * * * *', function() {
            var cron = new Cron('2 3 * * * *');
            expect(cron.cyclicField).to.be.equal(cron.fieldIndices.day);
        });

        it('m h d * * *', function() {
            var cron = new Cron('2 3 16 * * *');
            expect(cron.cyclicField).to.be.equal(cron.fieldIndices.month);
        });

        it('m h d M * *', function() {
            var cron = new Cron('2 3 16 2 * *');
            expect(cron.cyclicField).to.be.equal(cron.fieldIndices.year);
        });

        it('m h d M * Y', function() {
            var cron = new Cron('2 3 16 2 * 2016');
            expect(cron.cyclicField).to.be.undefined;
        });

        it('m h * M * *', function() {
            var cron = new Cron('2 3 * 2 * *');
            expect(cron.cyclicField).to.be.undefined;
        });

        it('m h * * w *', function() {
            var cron = new Cron('2 3 * * 5 *');
            expect(cron.cyclicField).to.be.equal(cron.fieldIndices.weekday);
        });

        it('m h d * w *', function() {
            var cron = new Cron('2 3 4 * 5 *');
            expect(cron.cyclicField).to.be.equal(cron.fieldIndices.month);
        });
    });

    describe('getNestedExecutionCount(date)', function() {
        it('* h * * * *', function() {
            var cron = new Cron('* 5 * * * *');
            expect(cron.getNestedExecutionCount(new Date())).to.be.equal(MINUTES_IN_HOUR);
        });

        it('m * d * * *', function() {
            var cron = new Cron('10 * 13 * * *');
            expect(cron.getNestedExecutionCount(new Date())).to.be.equal(HOURS_IN_DAY);
        });

        it('m h * M * *', function() {
            var cron = new Cron('35 4 * 6 * *');
            expect(cron.getNestedExecutionCount(new Date(2016, 5, 4))).to.be.equal(getMonthDaysCount(6, 2016));
        });

        it('m h d * * Y', function() {
            var cron = new Cron('35 4 11 * * 2016');
            expect(cron.getNestedExecutionCount(new Date())).to.be.equal(MONTHS_IN_YEAR);
        });

        it('* * d * * *', function() {
            var cron = new Cron('* * 6 * * *');
            expect(cron.getNestedExecutionCount(new Date())).to.be.equal(MINUTES_IN_HOUR * HOURS_IN_DAY);
        });

        it('* * * M * *', function() {
            var cron = new Cron('* * * 3 * *');

            expect(
                cron.getNestedExecutionCount(new Date(2016, 2, 4))
            ).to.be.equal(
                MINUTES_IN_HOUR * HOURS_IN_DAY * getMonthDaysCount(3, 2016)
            );
        });

        it('* * * * * Y', function() {
            var cron = new Cron('* * * * * 2015');

            expect(
                cron.getNestedExecutionCount(new Date(2015, 0, 1))
            ).to.be.equal(
                MINUTES_IN_HOUR * HOURS_IN_DAY * getYearDaysCount(2015)
            );
        });

        it('m * * M * *', function() {
            var cron = new Cron('3 * * 6 * *');

            expect(
                cron.getNestedExecutionCount(new Date(2015, 5, 1))
            ).to.be.equal(
                HOURS_IN_DAY * getMonthDaysCount(6, 2015)
            );
        });

        it('m * * * * Y', function() {
            var cron = new Cron('3 * * * * 2018');

            expect(
                cron.getNestedExecutionCount(new Date(2018, 3, 1))
            ).to.be.equal(
                HOURS_IN_DAY * getYearDaysCount(2018)
            );
        });

        it('m d * * * Y', function() {
            var cron = new Cron('26 * * * * 2019');

            expect(
                cron.getNestedExecutionCount(new Date(2018, 3, 1))
            ).to.be.equal(
                HOURS_IN_DAY * getYearDaysCount(2018)
            );
        });
    });
});
