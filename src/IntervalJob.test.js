var expect = require('chai').expect;
var sinon = require('sinon');

var IntervalJob = require('./IntervalJob');

describe('IntervalJob', function() {
    before(function() {
        this.clock = sinon.useFakeTimers(Date.now());
    });

    after(function() {
        this.clock.restore();
    });

    describe('interval: number', function() {
        beforeEach(function() {
            this.createIntervalJob = function(interval, func, options) {
                return this.intervalJob = new IntervalJob(interval, func, options);
            };
        });

        afterEach(function() {
            this.intervalJob.stop();
        });

        it('calls func after each interval', function() {
            var spy = sinon.spy();
            var interval = 364;
            this.createIntervalJob(interval, spy);

            this.clock.tick(interval);
            expect(spy.callCount).to.be.equal(1);

            this.clock.tick(interval);
            expect(spy.callCount).to.be.equal(2);
        });

        it('calls func with correct time', function(done) {
            var interval = 175;
            var expectedDate = new Date();
            expectedDate.setMilliseconds(expectedDate.getMilliseconds() + interval);

            this.createIntervalJob(interval, function() {
                expect(new Date()).to.be.deep.equal(expectedDate);
                done();
            });

            this.clock.tick(interval);
        });

        it('execution is stopped after the stop method call', function() {
            var spy = sinon.spy();
            var interval = 643;

            var intervalJob = this.createIntervalJob(interval, spy);
            intervalJob.stop();

            this.clock.tick(interval);
            expect(spy.callCount).to.be.equal(0);
        });

        it('execution is started with correct time if the startDate option is specified', function(done) {
            var interval = 435;
            var startDate = new Date();
            startDate.setMilliseconds(startDate.getMilliseconds() + interval * 20);

            this.createIntervalJob(interval, function() {
                expect(new Date()).to.be.deep.equal(startDate);
                done();
            }, { startDate: startDate });

            this.clock.tick(startDate.getTime() - Date.now());
        });

        it('execution is stopped after the stop method call if startDate is specified', function() {
            var spy = sinon.spy();
            var interval = 435;
            var startDate = new Date();
            startDate.setMilliseconds(startDate.getMilliseconds() + interval * 20);

            var intervalJob = this.createIntervalJob(interval, spy, { startDate: startDate });
            intervalJob.stop();

            this.clock.tick(startDate.getTime() - Date.now());
            expect(spy.callCount).to.be.equal(0);
        });
    });

    describe('interval: function', function() {
        beforeEach(function() {
            this.createIntervalJob = function(interval, func, options) {
                return this.intervalJob = new IntervalJob(interval, func, options);
            };
        });

        afterEach(function() {
            this.intervalJob.stop();
        });

        it('calls func after each interval', function() {
            var spy = sinon.spy();
            var intervals = [ 364, 286 ];
            var intervalIndex = 0;

            this.createIntervalJob(function() {
                return intervals[intervalIndex++];
            }, spy);

            this.clock.tick(intervals[0]);
            expect(spy.callCount).to.be.equal(1);

            this.clock.tick(intervals[1]);
            expect(spy.callCount).to.be.equal(2);
        });

        it('calls func with correct time', function(done) {
            var interval = 175;
            var expectedDate = new Date();
            expectedDate.setMilliseconds(expectedDate.getMilliseconds() + interval);

            this.createIntervalJob(
                function() {
                    return interval;
                },
                function() {
                    expect(new Date()).to.be.deep.equal(expectedDate);
                    done();
                }
            );

            this.clock.tick(interval);
        });

        it('execution is stopped after the stop method call', function() {
            var spy = sinon.spy();
            var interval = 643;

            var intervalJob = this.createIntervalJob(function() {
                return interval;
            }, spy);

            intervalJob.stop();

            this.clock.tick(interval);
            expect(spy.callCount).to.be.equal(0);
        });

        it('execution is started with correct time if the startDate option is specified', function(done) {
            var interval = 435;
            var startDate = new Date();
            startDate.setMilliseconds(startDate.getMilliseconds() + interval * 20);

            this.createIntervalJob(
                function() {
                    return interval;
                }, function() {
                    expect(new Date()).to.be.deep.equal(startDate);
                    done();
                },
                { startDate: startDate }
            );

            this.clock.tick(startDate.getTime() - Date.now());
        });

        it('execution is stopped after the stop method call if startDate is specified', function() {
            var spy = sinon.spy();
            var interval = 435;
            var startDate = new Date();
            startDate.setMilliseconds(startDate.getMilliseconds() + interval * 20);

            var intervalJob = this.createIntervalJob(function() {
                return interval;
            }, spy, { startDate: startDate });

            intervalJob.stop();

            this.clock.tick(startDate.getTime() - Date.now());
            expect(spy.callCount).to.be.equal(0);
        });
    });

    describe('useTimeout option', function() {
        beforeEach(function() {
            var originalSetInterval = setInterval;
            var originalSetTimeout = setTimeout;

            this.setIntervalStub = sinon.stub(global, 'setInterval', originalSetInterval);
            this.setTimeoutStub = sinon.stub(global, 'setTimeout', originalSetTimeout);

            this.createIntervalJob = function(interval, func, options) {
                return this.intervalJob = new IntervalJob(interval, func, options);
            };
        });

        afterEach(function() {
            this.intervalJob && this.intervalJob.stop();

            this.setTimeoutStub.restore();
            this.setIntervalStub.restore();
        });

        it('calls setInterval by default', function() {
            var interval = 275;
            this.createIntervalJob(interval, function() {});
            expect(this.setIntervalStub.callCount).to.be.equal(1);
            expect(this.setTimeoutStub.callCount).to.be.equal(0);
        });

        it('calls setInterval if useTimeout is false', function() {
            var interval = 635;
            this.createIntervalJob(interval, function() {}, { useTimeout: false });
            expect(this.setIntervalStub.callCount).to.be.equal(1);
            expect(this.setTimeoutStub.callCount).to.be.equal(0);
        });

        it('calls setTimeout if useTimeout is true', function() {
            var interval = 324;
            this.createIntervalJob(interval, function() {}, { useTimeout: true });
            expect(this.setTimeoutStub.callCount).to.be.equal(1);
            expect(this.setIntervalStub.callCount).to.be.equal(0);

            this.clock.tick(interval);
            expect(this.setTimeoutStub.callCount).to.be.equal(2);
            expect(this.setIntervalStub.callCount).to.be.equal(0);
        });
    });
});
