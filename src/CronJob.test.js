var expect = require('chai').expect;
var sinon = require('sinon');

var CronJob = require('./CronJob');

var MILLISECONDS_IN_SECOND = 1000;
var SECONDS_IN_MINUTE = 60;
var MINUTES_IN_HOUR = 60;
var HOURS_IN_DAY = 24;
var DAYS_IN_WEEK = 7;
var MONTHS_IN_YEAR = 12;

var MILLISECOND = 1;
var SECOND = MILLISECONDS_IN_SECOND * MILLISECOND;
var MINUTE = SECONDS_IN_MINUTE * SECOND;
var HOUR = MINUTES_IN_HOUR * MINUTE;
var DAY = HOURS_IN_DAY * HOUR;
var WEEK = DAYS_IN_WEEK * DAY;

function createCronByDate(date, fields) {
    var result = ['*', '*', '*', '*', '*', '*'];

    for(var i = 0, n = fields.length; i < n; i++) {
        switch(fields[i]) {
            case "minute":
                result[0] = date.getMinutes();
                break;
            case "hour":
                result[1] = date.getHours();
                break;
            case "day":
                result[2] = date.getDate();
                break;
            case "month":
                result[3] = date.getMonth() + 1;
                break;
            case "weekday":
                result[4] = date.getDay();

                if(result[4] === 0) {
                    result[4] = 7;
                }
                break;
            case "year":
                result[5] = date.getFullYear();
                break;
            default:
                break;
        }
    }

    return result.join(' ');
}

function getMonthDaysCount(month, year) {
    if(month === 2) {
        return (year % 4 > 0) ? 28 : 29;
    }

    if(
        (month <= 7 && month % 2 === 1)
        || (month > 7 && month % 2 === 0)
    ) {
        return 31;
    }

    return 30;
}

function getYearDaysCount(year) {
    return (year % 4 > 0) ? 365 : 366;
}

describe('CronJob', function() {
    before(function() {
        this.tickYear = function() {
            var finishDate = new Date();
            finishDate.setFullYear(finishDate.getFullYear() + 1);
            this.clock.tick(finishDate.getTime() - Date.now());
        };

        this.tickYears = function(count) {
            var finishDate = new Date();
            finishDate.setFullYear(finishDate.getFullYear() + count);
            this.clock.tick(finishDate.getTime() - Date.now());
        };
    });

    beforeEach(function() {
        this.date = new Date(2016, 7, 17, 15, 34);
        this.clock = sinon.useFakeTimers(this.date.getTime());
    });

    afterEach(function() {
        this.clock.restore();
    });

    describe('simple interval', function() {
        describe('* * * * * *', function() {
            beforeEach(function() {
                this.createCronJob = function(cron, func) {
                    return this.cronJob = new CronJob(cron, func);
                };
            });

            afterEach(function() {
                this.cronJob && this.cronJob.stop();
            });

            it('calls func every minute', function() {
                var callCount = 0;
                this.createCronJob('* * * * * *', function() { callCount++; });

                this.clock.tick(MINUTE);
                expect(callCount).to.be.equal(1);

                this.clock.tick(MINUTE);
                expect(callCount).to.be.equal(2);
            });

            it('calls func with correct second and millisecond', function(done) {
                this.createCronJob('* * * * * *', function() {
                    expect(Date.now() % MINUTE).to.be.equal(0);
                    done();
                });

                this.clock.tick(MINUTE);
            });
        });

        describe('m * * * * *', function() {
            beforeEach(function() {
                this.createCronJob = function(cron, func) {
                    return this.cronJob = new CronJob(cron, func);
                };
            });

            afterEach(function() {
                this.cronJob && this.cronJob.stop();
            });

            it('executes once an hour when specified in this hour', function () {
                var callCount = 0;
                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(expectedDate.getMinutes() + 12);

                var cron = createCronByDate(expectedDate, ['minute']);
                this.createCronJob(cron, function() { callCount++; });

                this.clock.tick(HOUR);
                expect(callCount).to.be.equal(1);
            });

            it('executes once an hour when specified in the next hour', function () {
                var callCount = 0;
                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(expectedDate.getMinutes() - 10);

                var cron = createCronByDate(expectedDate, ['minute']);
                this.createCronJob(cron, function() { callCount++ });

                this.clock.tick(HOUR);
                expect(callCount).to.be.equal(1);
            });

            it('executes with correct date when specified in this hour', function (done) {
                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(expectedDate.getMinutes() + 7);

                var cron = createCronByDate(expectedDate, ['minute']);

                this.createCronJob(cron, function () {
                    expect(new Date()).to.be.deep.equal(expectedDate);
                    done();
                });

                this.clock.tick(HOUR);
            });

            it('executes with correct date when specified the next hour', function (done) {
                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(expectedDate.getMinutes() - 15);
                expectedDate.setHours(expectedDate.getHours() + 1);

                var cron = createCronByDate(expectedDate, ['minute']);

                this.createCronJob(cron, function () {
                    expect(new Date()).to.be.deep.equal(expectedDate);
                    done();
                });

                this.clock.tick(HOUR);
            });
        });

        describe('m h * * * *', function() {
            beforeEach(function() {
                this.createCronJob = function(cron, func) {
                    return this.cronJob = new CronJob(cron, func);
                };
            });

            afterEach(function() {
                this.cronJob && this.cronJob.stop();
            });

            it('executes once a day when specified in this day', function () {
                var callCount = 0;

                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(34);
                expectedDate.setHours(expectedDate.getHours() + 6);

                var cron = createCronByDate(expectedDate, ['minute', 'hour']);
                this.createCronJob(cron, function() { callCount++; });

                this.clock.tick(DAY);
                expect(callCount).to.be.equal(1);
            });

            it('executes once a day when specified  in the next day', function () {
                var callCount = 0;

                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(34);
                expectedDate.setHours(expectedDate.getHours() - 4);

                var cron = createCronByDate(expectedDate, ['minute', 'hour']);
                this.createCronJob(cron, function() { callCount++; });

                this.clock.tick(DAY);
                expect(callCount).to.be.equal(1);
            });

            it('executes with correct date when specified in this day', function (done) {
                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(34);
                expectedDate.setHours(expectedDate.getHours() + 5);

                var cron = createCronByDate(expectedDate, ['minute', 'hour']);

                this.createCronJob(cron, function () {
                    expect(new Date()).to.be.deep.equal(expectedDate);
                    done();
                });

                this.clock.tick(DAY);
            });

            it('executes with correct date when specified in the next day', function (done) {
                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(34);
                expectedDate.setHours(expectedDate.getHours() - 2);
                expectedDate.setDate(expectedDate.getDate() + 1);

                var cron = createCronByDate(expectedDate, ['minute', 'hour']);

                this.createCronJob(cron, function () {
                    expect(new Date()).to.be.deep.equal(expectedDate);
                    done();
                });

                this.clock.tick(DAY);
            });
        });

        describe('m h d * * *', function() {
            beforeEach(function() {
                this.createCronJob = function(cron, func) {
                    return this.cronJob = new CronJob(cron, func);
                };
            });

            afterEach(function() {
                this.cronJob && this.cronJob.stop();
            });

            it('executes once a month when specified in this month', function() {
                var callCount = 0;

                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(54);
                expectedDate.setHours(3);
                expectedDate.setDate(expectedDate.getDate() - 4);

                var cron = createCronByDate(expectedDate, ['minute', 'hour', 'day']);
                this.createCronJob(cron, function() { callCount++; });

                var nextMonthDate = new Date(this.date);
                nextMonthDate.setMonth(nextMonthDate.getMonth() + 1);

                this.clock.tick(nextMonthDate.getTime() - this.date.getTime());
                expect(callCount).to.be.equal(1);
            });

            it('executes once a month when specified in the next month', function() {
                var callCount = 0;

                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(54);
                expectedDate.setHours(3);
                expectedDate.setDate(expectedDate.getDate() + 3);

                var cron = createCronByDate(expectedDate, ['minute', 'hour', 'day']);
                this.createCronJob(cron, function() { callCount++; });

                var nextMonthDate = new Date(this.date);
                nextMonthDate.setMonth(nextMonthDate.getMonth() + 1);

                this.clock.tick(nextMonthDate.getTime() - this.date.getTime());
                expect(callCount).to.be.equal(1);
            });

            it('executes with correct when specified in this month', function(done) {
                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(54);
                expectedDate.setHours(3);
                expectedDate.setDate(expectedDate.getDate() + 2);

                var cron = createCronByDate(expectedDate, ['minute', 'hour', 'day']);

                this.createCronJob(cron, function() {
                    expect(new Date()).to.be.deep.equal(expectedDate);
                    done();
                });

                var nextMonthDate = new Date(this.date);
                nextMonthDate.setMonth(nextMonthDate.getMonth() + 1);

                this.clock.tick(nextMonthDate.getTime() - this.date.getTime());
            });

            it('executes with correct when specified in the next month', function(done) {
                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(54);
                expectedDate.setHours(3);
                expectedDate.setDate(expectedDate.getDate() - 4);
                expectedDate.setMonth(expectedDate.getMonth() + 1);

                var cron = createCronByDate(expectedDate, ['minute', 'hour', 'day']);

                this.createCronJob(cron, function() {
                    expect(new Date()).to.be.deep.equal(expectedDate);
                    done();
                });

                var nextMonthDate = new Date(this.date);
                nextMonthDate.setMonth(nextMonthDate.getMonth() + 1);

                this.clock.tick(nextMonthDate.getTime() - this.date.getTime());
            });
        });

        describe('m h d M * *', function() {
            beforeEach(function() {
                this.createCronJob = function(cron, func) {
                    return this.cronJob = new CronJob(cron, func);
                };
            });

            afterEach(function() {
                this.cronJob && this.cronJob.stop();
            });

            it('executes once a year when specified in this year', function() {
                var callCount = 0;

                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(4);
                expectedDate.setHours(6);
                expectedDate.setDate(15);
                expectedDate.setMonth(expectedDate.getMonth() + 4);

                var cron = createCronByDate(expectedDate, ['minute', 'hour', 'day', 'month']);
                this.createCronJob(cron, function() { callCount++; });

                var nextYearDate = new Date(this.date);
                nextYearDate.setFullYear(nextYearDate.getFullYear() + 1);

                this.clock.tick(nextYearDate.getTime() - this.date.getTime());
                expect(callCount).to.be.equal(1);
            });

            it('executes once a year when specified in the next year', function() {
                var callCount = 0;

                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(4);
                expectedDate.setHours(6);
                expectedDate.setDate(15);
                expectedDate.setMonth(expectedDate.getMonth() - 3);
                expectedDate.setFullYear(expectedDate.getFullYear() + 1);

                var cron = createCronByDate(expectedDate, ['minute', 'hour', 'day', 'month']);
                this.createCronJob(cron, function() { callCount++; });

                var nextYearDate = new Date(this.date);
                nextYearDate.setFullYear(nextYearDate.getFullYear() + 1);

                this.clock.tick(nextYearDate.getTime() - this.date.getTime());
                expect(callCount).to.be.equal(1);
            });

            it('executes with correct date when specified in this year', function(done) {
                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(54);
                expectedDate.setHours(3);
                expectedDate.setDate(13);
                expectedDate.setMonth(expectedDate.getMonth() + 2);

                var cron = createCronByDate(expectedDate, ['minute', 'hour', 'day', 'month']);

                this.createCronJob(cron, function() {
                    expect(new Date()).to.be.deep.equal(expectedDate);
                    done();
                });

                var nextYearDate = new Date(this.date);
                nextYearDate.setFullYear(nextYearDate.getFullYear() + 1);

                this.clock.tick(nextYearDate.getTime() - this.date.getTime());
            });

            it('executes with correct date when specified in the next year', function(done) {
                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(54);
                expectedDate.setHours(3);
                expectedDate.setDate(13);
                expectedDate.setMonth(expectedDate.getMonth() - 3);
                expectedDate.setFullYear(expectedDate.getFullYear() + 1);

                var cron = createCronByDate(expectedDate, ['minute', 'hour', 'day', 'month']);

                this.createCronJob(cron, function() {
                    expect(new Date()).to.be.deep.equal(expectedDate);
                    done();
                });

                var nextYearDate = new Date(this.date);
                nextYearDate.setFullYear(nextYearDate.getFullYear() + 1);

                this.clock.tick(nextYearDate.getTime() - this.date.getTime());
            });
        });

        describe('m h * * w *', function() {
            beforeEach(function() {
                this.createCronJob = function(cron, func) {
                    return this.cronJob = new CronJob(cron, func);
                };
            });

            afterEach(function() {
                this.cronJob && this.cronJob.stop();
            });

            it('executes once a week when specified in this week', function () {
                var callCount = 0;

                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(34);
                expectedDate.setHours(4);
                expectedDate.setDate(expectedDate.getDate() + 2);

                var cron = createCronByDate(expectedDate, ['minute', 'hour', 'weekday']);
                this.createCronJob(cron, function() { callCount++; });

                this.clock.tick(WEEK);
                expect(callCount).to.be.equal(1);
            });

            it('executes once a week when specified in the next week', function () {
                var callCount = 0;

                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(51);
                expectedDate.setHours(3);
                expectedDate.setDate(expectedDate.getDate() - 1);

                var cron = createCronByDate(expectedDate, ['minute', 'hour', 'weekday']);
                this.createCronJob(cron, function() { callCount++; });

                this.clock.tick(WEEK);
                expect(callCount).to.be.equal(1);
            });

            it('executes with correct date when specified in this week', function (done) {
                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(30);
                expectedDate.setHours(20);
                expectedDate.setDate(expectedDate.getDate() + 1);

                var cron = createCronByDate(expectedDate, ['minute', 'hour', 'weekday']);

                this.createCronJob(cron, function () {
                    expect(new Date()).to.be.deep.equal(expectedDate);
                    done();
                });

                this.clock.tick(WEEK);
            });

            it('executes with correct date when specified in this week', function (done) {
                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(30);
                expectedDate.setHours(20);
                expectedDate.setDate(expectedDate.getDate() + 7 - 4);

                var cron = createCronByDate(expectedDate, ['minute', 'hour', 'weekday']);

                this.createCronJob(cron, function () {
                    expect(new Date()).to.be.deep.equal(expectedDate);
                    done();
                });

                this.clock.tick(WEEK);
            });
        });

        describe('0 0 * * w *', function() {
            beforeEach(function() {
                this.createCronJob = function(cron, func) {
                    return this.cronJob = new CronJob(cron, func);
                };
            });

            afterEach(function() {
                this.cronJob && this.cronJob.stop();
            });

            it('executes once a week when specified in this week', function () {
                var callCount = 0;

                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(0);
                expectedDate.setHours(0);
                expectedDate.setDate(expectedDate.getDate() + 2);

                var cron = createCronByDate(expectedDate, ['minute', 'hour', 'weekday']);
                this.createCronJob(cron, function() { callCount++; });

                this.clock.tick(WEEK);
                expect(callCount).to.be.equal(1);
            });

            it('executes once a week when specified in the next week', function () {
                var callCount = 0;

                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(0);
                expectedDate.setHours(0);
                expectedDate.setDate(expectedDate.getDate() + 7 - 1);

                var cron = createCronByDate(expectedDate, ['minute', 'hour', 'weekday']);
                this.createCronJob(cron, function() { callCount++ });

                this.clock.tick(WEEK);
                expect(callCount).to.be.equal(1);
            });

            it('executes with correct date when specified in this week', function (done) {
                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(0);
                expectedDate.setHours(0);
                expectedDate.setDate(expectedDate.getDate() + 1);

                var cron = createCronByDate(expectedDate, ['minute', 'hour', 'weekday']);

                this.createCronJob(cron, function () {
                    expect(new Date()).to.be.deep.equal(expectedDate);
                    done();
                });

                this.clock.tick(WEEK);
            });

            it('executes with correct date when specified in the next week', function (done) {
                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(0);
                expectedDate.setHours(0);
                expectedDate.setDate(expectedDate.getDate() + 7 - 2);

                var cron = createCronByDate(expectedDate, ['minute', 'hour', 'weekday']);

                this.createCronJob(cron, function () {
                    expect(new Date()).to.be.deep.equal(expectedDate);
                    done();
                });

                this.clock.tick(WEEK);
            });
        });
    });

    describe('simple split', function() {
        describe('* h * * * *', function() {
            beforeEach(function() {
                this.createCronJob = function(cron, func) {
                    return this.cronJob = new CronJob(cron, func);
                };
            });

            afterEach(function() {
                this.cronJob && this.cronJob.stop();
            });

            it('executes every minute', function() {
                var callCount = 0;

                var expectedDate = new Date(this.date);
                expectedDate.setHours(expectedDate.getHours() + 2);

                var cron = createCronByDate(expectedDate, ['hour']);
                this.createCronJob(cron, function() { callCount++; });

                this.clock.tick(DAY);
                expect(callCount).to.be.equal(MINUTES_IN_HOUR);
            });

            it('executes with correct date', function() {
                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(0);
                expectedDate.setHours(expectedDate.getHours() + 2);

                var cron = createCronByDate(expectedDate, ['hour']);

                this.createCronJob(cron, function() {
                    expect(new Date()).to.be.deep.equal(expectedDate);
                    expectedDate.setMinutes(expectedDate.getMinutes() + 1);
                });

                this.clock.tick(DAY);
            });
        });

        describe('* * d * * *', function() {
            beforeEach(function() {
                this.createCronJob = function(cron, func) {
                    return this.cronJob = new CronJob(cron, func);
                };
            });

            afterEach(function() {
                this.cronJob && this.cronJob.stop();
            });

            it('executes every minute', function() {
                var callCount = 0;

                var expectedDate = new Date(this.date);
                expectedDate.setDate(expectedDate.getDate() + 5);

                var cron = createCronByDate(expectedDate, ['day']);
                this.createCronJob(cron, function() {
                    callCount++;
                });

                var nextMonthDate = new Date(this.date);
                nextMonthDate.setMonth(nextMonthDate.getMonth() + 1);
                this.clock.tick(nextMonthDate.getTime() - this.date.getTime());

                expect(callCount).to.be.equal(MINUTES_IN_HOUR * HOURS_IN_DAY);
            });

            it('executes with correct date', function() {
                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(0);
                expectedDate.setHours(0);
                expectedDate.setDate(expectedDate.getDate() + 1);

                var cron = createCronByDate(expectedDate, ['day']);

                this.createCronJob(cron, function() {
                    expect(new Date()).to.be.deep.equal(expectedDate);
                    expectedDate.setMinutes(expectedDate.getMinutes() + 1);
                });

                var nextMonthDate = new Date(this.date);
                nextMonthDate.setMonth(nextMonthDate.getMonth() + 1);
                this.clock.tick(nextMonthDate.getTime() - this.date.getTime());
            });
        });

        describe('* * * * w *', function() {
            beforeEach(function() {
                this.createCronJob = function(cron, func) {
                    return this.cronJob = new CronJob(cron, func);
                };
            });

            afterEach(function() {
                this.cronJob && this.cronJob.stop();
            });

            it('executes every minute', function() {
                var callCount = 0;

                var expectedDate = new Date(this.date);
                expectedDate.setDate(expectedDate.getDate() + 2);

                var cron = createCronByDate(expectedDate, ['weekday']);
                this.createCronJob(cron, function() {
                    callCount++;
                });

                this.clock.tick(WEEK);
                expect(callCount).to.be.equal(MINUTES_IN_HOUR * HOURS_IN_DAY);
            });

            it('executes with correct date', function() {
                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(0);
                expectedDate.setHours(0);
                expectedDate.setDate(expectedDate.getDate() + 7 - 1);

                var cron = createCronByDate(expectedDate, ['weekday']);

                this.createCronJob(cron, function() {
                    expect(new Date()).to.be.deep.equal(expectedDate);
                    expectedDate.setMinutes(expectedDate.getMinutes() + 1);
                });

                this.clock.tick(WEEK);
            });
        });
    });

    describe('one split', function() {
        describe('side split', function() {
            describe('* h d * * *', function() {
                beforeEach(function() {
                    this.createCronJob = function(cron, func) {
                        return this.cronJob = new CronJob(cron, func);
                    };
                });

                afterEach(function() {
                    this.cronJob && this.cronJob.stop();
                });

                it('executes correct number of times', function() {
                    var callCount = 0;

                    var expectedDate = new Date(this.date);
                    expectedDate.setDate(expectedDate.getDate() + 5);

                    var cron = createCronByDate(expectedDate, ['hour', 'day']);

                    this.createCronJob(cron, function() {
                        callCount++;
                    });

                    var nextMonthDate = new Date(this.date);
                    nextMonthDate.setMonth(nextMonthDate.getMonth() + 1);
                    this.clock.tick(nextMonthDate.getTime() - this.date.getTime());

                    expect(callCount).to.be.equal(MINUTES_IN_HOUR);
                });

                it('executes with correct date', function() {
                    var expectedDate = new Date(
                        this.date.getFullYear(),
                        this.date.getMonth(),
                        this.date.getDate() + 4,
                        23
                    );

                    var cron = createCronByDate(expectedDate, ['hour', 'day']);

                    this.createCronJob(cron, function() {
                        expect(new Date()).to.be.deep.equal(expectedDate);
                        expectedDate.setMinutes(expectedDate.getMinutes() + 1);
                    });

                    var nextMonthDate = new Date(this.date);
                    nextMonthDate.setMonth(nextMonthDate.getMonth() + 1);
                    this.clock.tick(nextMonthDate.getTime() - this.date.getTime());
                });
            });

            describe('* * d M * *', function() {
                beforeEach(function() {
                    this.createCronJob = function(cron, func) {
                        return this.cronJob = new CronJob(cron, func);
                    };
                });

                afterEach(function() {
                    this.cronJob && this.cronJob.stop();
                });

                it('executes correct number of times', function() {
                    var callCount = 0;

                    var expectedDate = new Date(this.date);
                    expectedDate.setMonth(expectedDate.getMonth() + 3);

                    var cron = createCronByDate(expectedDate, ['day', 'month']);

                    this.createCronJob(cron, function() {
                        callCount++;
                    });

                    var nextYearDate = new Date(this.date);
                    nextYearDate.setFullYear(nextYearDate.getFullYear() + 1);
                    this.clock.tick(nextYearDate.getTime() - this.date.getTime());

                    expect(callCount).to.be.equal(MINUTES_IN_HOUR * HOURS_IN_DAY);
                });

                it('executes with correct date', function() {
                    var expectedDate = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 4);
                    var cron = createCronByDate(expectedDate, ['day', 'month']);

                    this.createCronJob(cron, function() {
                        expect(new Date()).to.be.deep.equal(expectedDate);
                        expectedDate.setMinutes(expectedDate.getMinutes() + 1);
                    });

                    var nextYearDate = new Date(this.date);
                    nextYearDate.setFullYear(nextYearDate.getFullYear() + 1);
                    this.clock.tick(nextYearDate.getTime() - this.date.getTime());
                });
            });

            describe('* h d M * *', function() {
                beforeEach(function() {
                    this.createCronJob = function(cron, func) {
                        return this.cronJob = new CronJob(cron, func);
                    };
                });

                afterEach(function() {
                    this.cronJob && this.cronJob.stop();
                });

                it('executes correct number of times', function() {
                    var expectedDate = new Date(this.date.getFullYear(), this.date.getMonth() + 3, 6, 13);
                    var cron = createCronByDate(expectedDate, ['hour', 'day', 'month']);

                    var callCount = 0;
                    this.createCronJob(cron, function() { callCount++; });

                    var nextYearDate = new Date(this.date);
                    nextYearDate.setFullYear(nextYearDate.getFullYear() + 1);
                    this.clock.tick(nextYearDate.getTime() - this.date.getTime());

                    expect(callCount).to.be.equal(MINUTES_IN_HOUR);
                });

                it('executes with correct date', function() {
                    var expectedDate = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 4, 1);
                    var cron = createCronByDate(expectedDate, ['hour', 'day', 'month']);

                    this.createCronJob(cron, function() {
                        expect(new Date()).to.be.deep.equal(expectedDate);
                        expectedDate.setMinutes(expectedDate.getMinutes() + 1);
                    });

                    var nextYearDate = new Date(this.date);
                    nextYearDate.setFullYear(nextYearDate.getFullYear() + 1);
                    this.clock.tick(nextYearDate.getTime() - this.date.getTime());
                });
            });

            describe('* * d M * Y', function() {
                beforeEach(function() {
                    this.createCronJob = function(cron, func) {
                        return this.cronJob = new CronJob(cron, func);
                    };
                });

                afterEach(function() {
                    this.cronJob && this.cronJob.stop();
                });

                it('executes correct number of times', function() {
                    var expectedDate = new Date(this.date.getFullYear() + 1, this.date.getMonth() + 3, 6);
                    var cron = createCronByDate(expectedDate, ['day', 'month', 'year']);

                    var callCount = 0;
                    this.createCronJob(cron, function() { callCount++; });

                    var nextDecadeDate = new Date(this.date);
                    nextDecadeDate.setFullYear(nextDecadeDate.getFullYear() + 10);
                    this.clock.tick(nextDecadeDate.getTime() - this.date.getTime());

                    expect(callCount).to.be.equal(MINUTES_IN_HOUR * HOURS_IN_DAY);
                });

                it('executes with correct date', function() {
                    var expectedDate = new Date(this.date.getFullYear() + 1, this.date.getMonth() + 1, 4);
                    var cron = createCronByDate(expectedDate, ['day', 'month', 'year']);

                    this.createCronJob(cron, function() {
                        expect(new Date()).to.be.deep.equal(expectedDate);
                        expectedDate.setMinutes(expectedDate.getMinutes() + 1);
                    });

                    var nextDecadeDate = new Date(this.date);
                    nextDecadeDate.setFullYear(nextDecadeDate.getFullYear() + 10);
                    this.clock.tick(nextDecadeDate.getTime() - this.date.getTime());
                });
            });

            describe('* h * * w *', function() {
                beforeEach(function() {
                    this.createCronJob = function(cron, func) {
                        return this.cronJob = new CronJob(cron, func);
                    };
                });

                afterEach(function() {
                    this.cronJob && this.cronJob.stop();
                });

                it('executes correct number of times', function() {
                    var callCount = 0;

                    var expectedDate = new Date(this.date);
                    expectedDate.setHours(3);
                    expectedDate.setDate(expectedDate.getDate() + 2);

                    var cron = createCronByDate(expectedDate, ['hour', 'weekday']);

                    this.createCronJob(cron, function() {
                        callCount++;
                    });

                    this.clock.tick(WEEK);
                    expect(callCount).to.be.equal(MINUTES_IN_HOUR);
                });

                it('executes with correct date', function() {
                    var expectedDate = new Date(this.date);
                    expectedDate.setMilliseconds(0);
                    expectedDate.setSeconds(0);
                    expectedDate.setMinutes(0);
                    expectedDate.setHours(12);
                    expectedDate.setDate(expectedDate.getDate() + 3);

                    var cron = createCronByDate(expectedDate, ['hour', 'weekday']);

                    this.createCronJob(cron, function() {
                        expect(new Date()).to.be.deep.equal(expectedDate);
                        expectedDate.setMinutes(expectedDate.getMinutes() + 1);
                    });

                    this.clock.tick(WEEK);
                });
            });
        });

        describe('central split', function() {
            describe('m * d * * *', function() {
                beforeEach(function() {
                    this.createCronJob = function(cron, func) {
                        return this.cronJob = new CronJob(cron, func);
                    };
                });

                afterEach(function() {
                    this.cronJob && this.cronJob.stop();
                });

                it('executes correct number of times', function() {
                    var expectedDate = new Date(
                        this.date.getFullYear(),
                        this.date.getMonth(),
                        this.date.getDate() + 2,
                        43
                    );

                    var cron = createCronByDate(expectedDate, ['minute', 'day']);

                    var callCount = 0;
                    this.createCronJob(cron, function() { callCount++; });

                    var nextMonthDate = new Date(this.date);
                    nextMonthDate.setMonth(nextMonthDate.getMonth() + 1);
                    this.clock.tick(nextMonthDate.getTime() - this.date.getTime());

                    expect(callCount).to.be.equal(HOURS_IN_DAY);
                });

                it('executes with correct date', function() {
                    var expectedDate = new Date(
                        this.date.getFullYear(),
                        this.date.getMonth(),
                        this.date.getDate() + 3,
                        0,
                        22
                    );

                    var cron = createCronByDate(expectedDate, ['minute', 'day']);

                    this.createCronJob(cron, function() {
                        expect(new Date()).to.be.deep.equal(expectedDate);
                        expectedDate.setHours(expectedDate.getHours() + 1);
                    });

                    var nextMonthDate = new Date(this.date);
                    nextMonthDate.setMonth(nextMonthDate.getMonth() + 1);
                    this.clock.tick(nextMonthDate.getTime() - this.date.getTime());
                });
            });

            describe('m * d M * *', function() {
                beforeEach(function() {
                    this.createCronJob = function(cron, func) {
                        return this.cronJob = new CronJob(cron, func);
                    };
                });

                afterEach(function() {
                    this.cronJob && this.cronJob.stop();
                });

                it('executes correct number of times', function() {
                    var expectedDate = new Date(this.date.getFullYear(), this.date.getMonth() + 5, 20, 0, 53);
                    var cron = createCronByDate(expectedDate, ['minute', 'day', 'month']);

                    var callCount = 0;
                    this.createCronJob(cron, function() { callCount++; });

                    var nextYearDate = new Date(this.date);
                    nextYearDate.setFullYear(nextYearDate.getFullYear() + 1);
                    this.clock.tick(nextYearDate.getTime() - this.date.getTime());

                    expect(callCount).to.be.equal(HOURS_IN_DAY);
                });

                it('executes with correct date', function() {
                    var expectedDate = new Date(this.date.getFullYear(), this.date.getMonth() + 5, 20, 0, 53);
                    var cron = createCronByDate(expectedDate, ['minute', 'day', 'month']);

                    this.createCronJob(cron, function() {
                        expect(new Date()).to.be.deep.equal(expectedDate);
                        expectedDate.setHours(expectedDate.getHours() + 1);
                    });

                    var nextYearDate = new Date(this.date);
                    nextYearDate.setFullYear(nextYearDate.getFullYear() + 1);
                    this.clock.tick(nextYearDate.getTime() - this.date.getTime());
                });
            });

            describe('m * d M * Y', function() {
                beforeEach(function() {
                    this.createCronJob = function(cron, func) {
                        return this.cronJob = new CronJob(cron, func);
                    };
                });

                afterEach(function() {
                    this.cronJob && this.cronJob.stop();
                });

                it('executes correct number of times', function() {
                    var expectedDate = new Date(this.date.getFullYear() + 3, 1, 20, 0, 53);
                    var cron = createCronByDate(expectedDate, ['minute', 'day', 'month', 'year']);

                    var callCount = 0;
                    this.createCronJob(cron, function() { callCount++; });

                    var nextDecadeDate = new Date(this.date);
                    nextDecadeDate.setFullYear(nextDecadeDate.getFullYear() + 10);
                    this.clock.tick(nextDecadeDate.getTime() - this.date.getTime());

                    expect(callCount).to.be.equal(HOURS_IN_DAY);
                });

                it('executes with correct date', function() {
                    var expectedDate = new Date(this.date.getFullYear() + 6, 10, 20, 0, 53);
                    var cron = createCronByDate(expectedDate, ['minute', 'day', 'month', 'year']);

                    this.createCronJob(cron, function() {
                        expect(new Date()).to.be.deep.equal(expectedDate);
                        expectedDate.setHours(expectedDate.getHours() + 1);
                    });

                    var nextDecadeDate = new Date(this.date);
                    nextDecadeDate.setFullYear(nextDecadeDate.getFullYear() + 10);
                    this.clock.tick(nextDecadeDate.getTime() - this.date.getTime());
                });
            });

            describe('m h * M * *', function() {
                beforeEach(function() {
                    this.createCronJob = function(cron, func) {
                        return this.cronJob = new CronJob(cron, func);
                    };
                });

                afterEach(function() {
                    this.cronJob && this.cronJob.stop();
                });

                it('executes correct number of times', function() {
                    var expectedDate = new Date(this.date.getFullYear(), this.date.getMonth() + 3, 1, 8, 40);
                    var cron = createCronByDate(expectedDate, ['minute', 'hour', 'month']);

                    var callCount = 0;
                    this.createCronJob(cron, function() { callCount++; });

                    var nextYearDate = new Date(this.date);
                    nextYearDate.setFullYear(nextYearDate.getFullYear() + 1);
                    this.clock.tick(nextYearDate.getTime() - this.date.getTime());

                    var daysInMonth = getMonthDaysCount(expectedDate.getMonth() + 1, expectedDate.getFullYear());
                    expect(callCount).to.be.equal(daysInMonth);
                });

                it('executes with correct date', function() {
                    var expectedDate = new Date(this.date.getFullYear(), this.date.getMonth() + 3, 1, 8, 40);
                    var cron = createCronByDate(expectedDate, ['minute', 'hour', 'month']);

                    this.createCronJob(cron, function() {
                        expect(new Date()).to.be.deep.equal(expectedDate);
                        expectedDate.setDate(expectedDate.getDate() + 1);
                    });

                    var nextYearDate = new Date(this.date);
                    nextYearDate.setFullYear(nextYearDate.getFullYear() + 1);
                    this.clock.tick(nextYearDate.getTime() - this.date.getTime());
                });
            });

            describe('m h * M * Y', function() {
                beforeEach(function() {
                    this.createCronJob = function(cron, func) {
                        return this.cronJob = new CronJob(cron, func);
                    };
                });

                afterEach(function() {
                    this.cronJob && this.cronJob.stop();
                });

                it('executes correct number of times', function() {
                    var expectedDate = new Date(this.date.getFullYear() + 3, 11, 1, 8, 40);
                    var cron = createCronByDate(expectedDate, ['minute', 'hour', 'month', 'year']);

                    var callCount = 0;
                    this.createCronJob(cron, function() { callCount++; });

                    var nextDecadeDate = new Date(this.date);
                    nextDecadeDate.setFullYear(nextDecadeDate.getFullYear() + 10);
                    this.clock.tick(nextDecadeDate.getTime() - this.date.getTime());

                    var daysInMonth = getMonthDaysCount(expectedDate.getMonth() + 1, expectedDate.getFullYear());
                    expect(callCount).to.be.equal(daysInMonth);
                });

                it('executes with correct date', function() {
                    var expectedDate = new Date(this.date.getFullYear() + 1, 2, 1, 8, 40);
                    var cron = createCronByDate(expectedDate, ['minute', 'hour', 'month', 'year']);

                    this.createCronJob(cron, function() {
                        expect(new Date()).to.be.deep.equal(expectedDate);
                        expectedDate.setDate(expectedDate.getDate() + 1);
                    });

                    var nextDecadeDate = new Date(this.date);
                    nextDecadeDate.setFullYear(nextDecadeDate.getFullYear() + 10);
                    this.clock.tick(nextDecadeDate.getTime() - this.date.getTime());
                });
            });

            describe('m h d * * Y', function() {
                beforeEach(function() {
                    this.createCronJob = function(cron, func) {
                        return this.cronJob = new CronJob(cron, func);
                    };
                });

                afterEach(function() {
                    this.cronJob && this.cronJob.stop();
                });

                it('executes correct number of times', function() {
                    var expectedDate = new Date(this.date.getFullYear() + 3, 0, 15, 8, 40);
                    var cron = createCronByDate(expectedDate, ['minute', 'hour', 'day', 'year']);

                    var callCount = 0;
                    this.createCronJob(cron, function() { callCount++; });

                    var nextDecadeDate = new Date(this.date);
                    nextDecadeDate.setFullYear(nextDecadeDate.getFullYear() + 10);
                    this.clock.tick(nextDecadeDate.getTime() - this.date.getTime());
                    expect(callCount).to.be.equal(MONTHS_IN_YEAR);
                });

                it('executes with correct date', function() {
                    var expectedDate = new Date(this.date.getFullYear() + 7, 0, 5, 8, 40);
                    var cron = createCronByDate(expectedDate, ['minute', 'hour', 'day', 'year']);

                    this.createCronJob(cron, function() {
                        expect(new Date()).to.be.deep.equal(expectedDate);
                        expectedDate.setMonth(expectedDate.getMonth() + 1);
                    });

                    var nextDecadeDate = new Date(this.date);
                    nextDecadeDate.setFullYear(nextDecadeDate.getFullYear() + 10);
                    this.clock.tick(nextDecadeDate.getTime() - this.date.getTime());
                });
            });

            describe('m * * * w *', function() {
                beforeEach(function() {
                    this.createCronJob = function(cron, func) {
                        return this.cronJob = new CronJob(cron, func);
                    };
                });

                afterEach(function() {
                    this.cronJob && this.cronJob.stop();
                });

                it('executes correct number of times', function() {
                    var callCount = 0;

                    var expectedDate = new Date(this.date);
                    expectedDate.setMinutes(34);
                    expectedDate.setDate(expectedDate.getDate() + 2);

                    var cron = createCronByDate(expectedDate, ['minute', 'weekday']);

                    this.createCronJob(cron, function() {
                        callCount++;
                    });

                    this.clock.tick(WEEK);
                    expect(callCount).to.be.equal(HOURS_IN_DAY);
                });

                it('executes with correct date', function() {
                    var expectedDate = new Date(this.date);
                    expectedDate.setMilliseconds(0);
                    expectedDate.setSeconds(0);
                    expectedDate.setMinutes(54);
                    expectedDate.setHours(0);
                    expectedDate.setDate(expectedDate.getDate() + 1);

                    var cron = createCronByDate(expectedDate, ['minute', 'weekday']);

                    this.createCronJob(cron, function() {
                        expect(new Date()).to.be.deep.equal(expectedDate);
                        expectedDate.setHours(expectedDate.getHours() + 1);
                    });

                    this.clock.tick(WEEK);
                });
            });
        });
    });

    describe('two splits', function() {
        beforeEach(function() {
            this.createCronJob = function(cron, func) {
                return this.cronJob = new CronJob(cron, func);
            };
        });

        afterEach(function() {
            this.cronJob && this.cronJob.stop();
        });

        describe('* h * M * *', function() {
            it('executes correct number of times', function() {
                var now = new Date();
                var expectedDate = new Date(now.getFullYear(), now.getMonth() + 3, 1, 16);
                var cron = createCronByDate(expectedDate, ['hour', 'month']);

                var callCount = 0;
                this.createCronJob(cron, function() { callCount++; });

                var nextYearDate = new Date();
                nextYearDate.setFullYear(nextYearDate.getFullYear() + 1);
                this.clock.tick(nextYearDate.getTime() - Date.now());

                var daysInMonth = getMonthDaysCount(expectedDate.getMonth() + 1, expectedDate.getYear());
                expect(callCount).to.be.equal(MINUTES_IN_HOUR * daysInMonth);
            });

            it('executes with correct date', function() {
                var now = new Date();
                var expectedDate = new Date(now.getFullYear(), now.getMonth() + 3, 1, 16);
                var cron = createCronByDate(expectedDate, ['hour', 'month']);

                this.createCronJob(cron, function() {
                    expect(new Date()).to.be.deep.equal(expectedDate);

                    if(expectedDate.getMinutes() < MINUTES_IN_HOUR - 1) {
                        expectedDate.setMinutes(expectedDate.getMinutes() + 1);
                    } else {
                        expectedDate.setMinutes(0);
                        expectedDate.setDate(expectedDate.getDate() + 1);
                    }
                });

                var nextYearDate = new Date();
                nextYearDate.setFullYear(nextYearDate.getFullYear() + 1);
                this.clock.tick(nextYearDate.getTime() - Date.now());
            });
        });

        describe('* h * * * Y', function() {
            it('executes correct number of times', function() {
                var now = new Date();
                var expectedDate = new Date(now.getFullYear() + 2, 0, 1, 8);
                var cron = createCronByDate(expectedDate, ['hour', 'year']);

                var callCount = 0;
                this.createCronJob(cron, function() { callCount++; });

                var nextDecadeDate = new Date();
                nextDecadeDate.setFullYear(nextDecadeDate.getFullYear() + 10);
                this.clock.tick(nextDecadeDate.getTime() - Date.now());

                var daysInYear = getYearDaysCount(expectedDate.getFullYear());
                expect(callCount).to.be.equal(MINUTES_IN_HOUR * daysInYear);
            });

            it('executes with correct date', function() {
                var now = new Date();
                var expectedDate = new Date(now.getFullYear() + 3, 0, 1, 21);
                var cron = createCronByDate(expectedDate, ['hour', 'year']);

                this.createCronJob(cron, function() {
                    expect(new Date()).to.be.deep.equal(expectedDate);

                    if(expectedDate.getMinutes() < MINUTES_IN_HOUR - 1) {
                        expectedDate.setMinutes(expectedDate.getMinutes() + 1);
                    } else {
                        expectedDate.setMinutes(0);
                        expectedDate.setDate(expectedDate.getDate() + 1);
                    }
                });

                var nextDecadeDate = new Date();
                nextDecadeDate.setFullYear(nextDecadeDate.getFullYear() + 10);
                this.clock.tick(nextDecadeDate.getTime() - Date.now());
            });
        });

        describe('* h * M * Y', function() {
            it('executes correct number of times', function() {
                var now = new Date();
                var expectedDate = new Date(now.getFullYear() + 2, 6, 1, 8);
                var cron = createCronByDate(expectedDate, ['hour', 'month', 'year']);

                var callCount = 0;
                this.createCronJob(cron, function() { callCount++; });

                var nextDecadeDate = new Date();
                nextDecadeDate.setFullYear(nextDecadeDate.getFullYear() + 10);
                this.clock.tick(nextDecadeDate.getTime() - Date.now());

                var daysInMonth = getMonthDaysCount(expectedDate.getMonth() + 1, expectedDate.getFullYear());
                expect(callCount).to.be.equal(MINUTES_IN_HOUR * daysInMonth);
            });

            it('executes with correct date', function() {
                var now = new Date();
                var expectedDate = new Date(now.getFullYear() + 3, 4, 1, 21);
                var cron = createCronByDate(expectedDate, ['hour', 'month', 'year']);

                this.createCronJob(cron, function() {
                    expect(new Date()).to.be.deep.equal(expectedDate);

                    if(expectedDate.getMinutes() < MINUTES_IN_HOUR - 1) {
                        expectedDate.setMinutes(expectedDate.getMinutes() + 1);
                    } else {
                        expectedDate.setMinutes(0);
                        expectedDate.setDate(expectedDate.getDate() + 1);
                    }
                });

                var nextDecadeDate = new Date();
                nextDecadeDate.setFullYear(nextDecadeDate.getFullYear() + 10);
                this.clock.tick(nextDecadeDate.getTime() - Date.now());
            });
        });

        describe('* h d * * Y', function() {
            it('executes correct number of times', function() {
                var now = new Date();
                var expectedDate = new Date(now.getFullYear() + 2, 0, 10, 8);
                var cron = createCronByDate(expectedDate, ['hour', 'day', 'year']);

                var callCount = 0;
                this.createCronJob(cron, function() { callCount++; });

                var nextDecadeDate = new Date();
                nextDecadeDate.setFullYear(nextDecadeDate.getFullYear() + 10);
                this.clock.tick(nextDecadeDate.getTime() - Date.now());

                expect(callCount).to.be.equal(MINUTES_IN_HOUR * MONTHS_IN_YEAR);
            });

            it('executes with correct date', function() {
                var now = new Date();
                var expectedDate = new Date(now.getFullYear() + 3, 0, 11, 21);
                var cron = createCronByDate(expectedDate, ['hour', 'day', 'year']);

                this.createCronJob(cron, function() {
                    expect(new Date()).to.be.deep.equal(expectedDate);

                    if(expectedDate.getMinutes() < MINUTES_IN_HOUR - 1) {
                        expectedDate.setMinutes(expectedDate.getMinutes() + 1);
                    } else {
                        expectedDate.setMinutes(0);
                        expectedDate.setMonth(expectedDate.getMonth() + 1);
                    }
                });

                var nextDecadeDate = new Date();
                nextDecadeDate.setFullYear(nextDecadeDate.getFullYear() + 10);
                this.clock.tick(nextDecadeDate.getTime() - Date.now());
            });
        });

        describe('m * d * * Y', function() {
            it('executes correct number of times', function() {
                var now = new Date();
                var expectedDate = new Date(now.getFullYear() + 2, 0, 10, 0, 46);
                var cron = createCronByDate(expectedDate, ['minute', 'day', 'year']);

                var callCount = 0;
                this.createCronJob(cron, function() { callCount++; });

                var nextDecadeDate = new Date();
                nextDecadeDate.setFullYear(nextDecadeDate.getFullYear() + 10);
                this.clock.tick(nextDecadeDate.getTime() - Date.now());

                expect(callCount).to.be.equal(HOURS_IN_DAY * MONTHS_IN_YEAR);
            });

            it('executes with correct date', function() {
                var now = new Date();
                var expectedDate = new Date(now.getFullYear() + 3, 0, 11, 0, 21);
                var cron = createCronByDate(expectedDate, ['minute', 'day', 'year']);

                this.createCronJob(cron, function() {
                    expect(new Date()).to.be.deep.equal(expectedDate);

                    if(expectedDate.getHours() < HOURS_IN_DAY - 1) {
                        expectedDate.setHours(expectedDate.getHours() + 1);
                    } else {
                        expectedDate.setHours(0);
                        expectedDate.setMonth(expectedDate.getMonth() + 1);
                    }
                });

                var nextDecadeDate = new Date();
                nextDecadeDate.setFullYear(nextDecadeDate.getFullYear() + 10);
                this.clock.tick(nextDecadeDate.getTime() - Date.now());
            });
        });
    });

    describe('conditional fields', function() {
        describe('m h 30 * * *', function() {
            beforeEach(function() {
                this.createCronJob = function(cron, func) {
                    return this.cronJob = new CronJob(cron, func);
                };
            });

            afterEach(function() {
                this.cronJob && this.cronJob.stop();
            });

            it('executes with correct number of times', function() {
                var callCount = 0;

                this.createCronJob('54 23 30 * * *', function() {
                    callCount++;
                });

                this.tickYear();

                // Exclude February
                expect(callCount).to.be.equal(MONTHS_IN_YEAR - 1);
            });

            it('executes with correct date', function() {
                var now = new Date();

                var expectedDate = new Date(
                    now.getFullYear(),
                    now.getMonth(),
                    30,
                    23,
                    54
                );

                this.createCronJob('54 23 30 * * *', function() {
                    expect(new Date()).to.be.deep.equal(expectedDate);
                    expectedDate.setMonth(expectedDate.getMonth() + (expectedDate.getMonth() === 0 ? 2 : 1));
                });

                this.tickYear();
            });
        });

        describe('m h 31 * * *', function() {
            beforeEach(function() {
                this.createCronJob = function(cron, func) {
                    return this.cronJob = new CronJob(cron, func);
                };
            });

            afterEach(function() {
                this.cronJob && this.cronJob.stop();
            });

            it('executes with correct number of times', function() {
                var callCount = 0;

                this.createCronJob('54 23 31 * * *', function() {
                    callCount++;
                });

                this.tickYear();

                // January, March, May, July, August, October, December
                expect(callCount).to.be.equal(7);
            });

            it('executes with correct date', function() {
                var now = new Date();

                var expectedDate = new Date(
                    now.getFullYear(),
                    now.getMonth(),
                    31,
                    23,
                    54
                );

                this.createCronJob('54 23 31 * * *', function() {
                    expect(new Date()).to.be.deep.equal(expectedDate);

                    var nextMonth = expectedDate.getMonth() + 1;

                    if(getMonthDaysCount(nextMonth + 1, expectedDate.getFullYear()) < 31) {
                        nextMonth++;
                    }

                    expectedDate.setMonth(nextMonth);
                });

                this.tickYear();
            });
        });

        describe('m h 29 2 * *', function() {
            beforeEach(function() {
                this.createCronJob = function(cron, func) {
                    return this.cronJob = new CronJob(cron, func);
                };
            });

            afterEach(function() {
                this.cronJob && this.cronJob.stop();
            });

            it('executes with correct number of times', function() {
                var callCount = 0;

                this.createCronJob('54 23 29 2 * *', function() {
                    callCount++;
                });

                this.tickYears(4);
                expect(callCount).to.be.equal(1);
            });

            it('executes with correct date', function() {
                var expectedDate = new Date(2020, 1, 29, 23, 54);

                this.createCronJob('54 23 29 2 * *', function() {
                    expect(new Date()).to.be.deep.equal(expectedDate);
                });

                this.tickYears(4);
            });
        });

        describe('m h d * w *', function() {
            beforeEach(function() {
                this.createCronJob = function(cron, func) {
                    return this.cronJob = new CronJob(cron, func);
                };
            });

            afterEach(function() {
                this.cronJob && this.cronJob.stop();
            });

            it('executes with correct number of times', function() {
                var callCount = 0;

                var expectedDates = [
                    new Date(2016, 9, 5), // 05.10.16 Wen
                    new Date(2017, 3, 5), // 05.04.17 Wen
                    new Date(2017, 6, 5)  // 05.07.17 Wen
                ];

                this.createCronJob('3 16 5 * 3 *', function() {
                    callCount++;
                });

                this.tickYear();
                expect(callCount).to.be.equal(expectedDates.length);
            });

            it('executes with correct date', function() {
                var dateIndex = 0;

                var expectedDates = [
                    new Date(2016, 7, 20, 16, 3), // 20.08.16 Sat
                    new Date(2017, 4, 20, 16, 3)  // 20.05.17 Sat
                ];

                this.createCronJob('3 16 20 * 6 *', function() {
                    expect(new Date()).to.be.deep.equal(expectedDates[dateIndex]);
                    dateIndex++;
                });

                this.tickYear();
            });
        });
    });

    describe('one execution', function() {
        describe('m h d M * Y', function() {
            beforeEach(function() {
                this.createCronJob = function(cron, func) {
                    return this.cronJob = new CronJob(cron, func);
                };
            });

            afterEach(function() {
                this.cronJob && this.cronJob.stop();
            });

            it('executes with correct date', function(done) {
                var expectedDate = new Date(this.date);
                expectedDate.setMinutes(54);
                expectedDate.setHours(3);
                expectedDate.setDate(13);
                expectedDate.setMonth(5);
                expectedDate.setFullYear(expectedDate.getFullYear() + 1);

                var cron = createCronByDate(expectedDate, ['minute', 'hour', 'day', 'month', 'year']);

                this.createCronJob(cron, function() {
                    expect(new Date()).to.be.deep.equal(expectedDate);
                    done();
                });

                var nextYearDate = new Date(this.date);
                nextYearDate.setFullYear(nextYearDate.getFullYear() + 2);

                this.clock.tick(nextYearDate.getTime() - this.date.getTime());
            });
        });

        describe('0 0 1 1 * Y', function() {
            beforeEach(function() {
                this.createCronJob = function(cron, func) {
                    return this.cronJob = new CronJob(cron, func);
                };
            });

            afterEach(function() {
                this.cronJob && this.cronJob.stop();
            });

            it('executes with correct date', function(done) {
                var expectedDate = new Date(this.date.getFullYear() + 1, 0, 1);
                var cron = createCronByDate(expectedDate, ['minute', 'hour', 'day', 'month', 'year']);

                this.createCronJob(cron, function() {
                    expect(new Date()).to.be.deep.equal(expectedDate);
                    done();
                });

                var nextYearDate = new Date(this.date);
                nextYearDate.setFullYear(nextYearDate.getFullYear() + 2);

                this.clock.tick(nextYearDate.getTime() - this.date.getTime());
            });
        });
    });

    describe('unreal scenarios (hope they will never be used)', function() {
        describe('* * * M * * (every minute in specified month - rethink your algorithm logic)', function() {
            beforeEach(function() {
                this.createCronJob = function(cron, func) {
                    return this.cronJob = new CronJob(cron, func);
                };
            });

            afterEach(function() {
                this.cronJob && this.cronJob.stop();
            });

            it('does not execute before', function() {
                var expectedDate = new Date(this.date.getFullYear(), this.date.getMonth() + 11, 1);
                var cron = createCronByDate(expectedDate, ['month']);

                var callCount = 0;
                this.createCronJob(cron, function() { callCount++; });

                this.clock.tick(expectedDate.getTime() - this.date.getTime() - 1);
                expect(callCount).to.be.equal(0);

                this.clock.tick(1);
                expect(callCount).to.be.equal(1);
            });

            it('does not execute after', function() {
                var expectedDate = new Date(this.date.getFullYear(), this.date.getMonth() - 1, 1);
                var cron = createCronByDate(expectedDate, ['month']);

                var callCount = 0;
                this.createCronJob(cron, function() { callCount++; });

                var nextYearDate = new Date(expectedDate);
                nextYearDate.setFullYear(nextYearDate.getFullYear() + 1);
                nextYearDate.setMonth(nextYearDate.getMonth());

                this.clock.tick(nextYearDate.getTime() - this.date.getTime() - 1);
                expect(callCount).to.be.equal(0);

                this.clock.tick(1);
                expect(callCount).to.be.equal(1);
            });

            it('executes every minute (trust me)', function() {
                var expectedDate = new Date(this.date.getFullYear(), this.date.getMonth() + 4, 1);
                var cron = createCronByDate(expectedDate, ['month']);

                var callCount = 0;
                this.createCronJob(cron, function() { callCount++; });

                this.clock.tick(expectedDate.getTime() - this.date.getTime() - 1 + HOUR);
                expect(callCount).to.be.equal(MINUTES_IN_HOUR);
            });
        });

        describe('* * * * * Y (every minute in specified year - are you serious?)', function() {
            beforeEach(function() {
                this.createCronJob = function(cron, func) {
                    return this.cronJob = new CronJob(cron, func);
                };
            });

            afterEach(function() {
                this.cronJob && this.cronJob.stop();
            });

            it('does not execute before year', function() {
                var expectedDate = new Date(this.date.getFullYear() + 10, 0, 1);
                var cron = createCronByDate(expectedDate, ['year']);

                var callCount = 0;
                this.createCronJob(cron, function() { callCount++; });

                this.clock.tick(expectedDate.getTime() - this.date.getTime() - 1);
                expect(callCount).to.be.equal(0);

                this.clock.tick(1);
                expect(callCount).to.be.equal(1);
            });

            it('executes every minute (checked this twice)', function() {
                var expectedDate = new Date(this.date.getFullYear() + 3, 0, 1);
                var cron = createCronByDate(expectedDate, ['year']);

                var callCount = 0;
                this.createCronJob(cron, function() { callCount++; });

                this.clock.tick(expectedDate.getTime() - this.date.getTime() - 1 + HOUR);
                expect(callCount).to.be.equal(MINUTES_IN_HOUR);
            });
        });

        describe('* * * M * Y (every minute in specified month and year - why do you need it?)', function() {
            beforeEach(function() {
                this.createCronJob = function(cron, func) {
                    return this.cronJob = new CronJob(cron, func);
                };
            });

            afterEach(function() {
                this.cronJob && this.cronJob.stop();
            });

            it('does not execute before', function() {
                var expectedDate = new Date(this.date.getFullYear() + 4, 6, 1);
                var cron = createCronByDate(expectedDate, ['month', 'year']);

                var callCount = 0;
                this.createCronJob(cron, function() { callCount++; });

                this.clock.tick(expectedDate.getTime() - this.date.getTime() - 1);
                expect(callCount).to.be.equal(0);

                this.clock.tick(1);
                expect(callCount).to.be.equal(1);
            });

            it('executes every minute (1000%)', function() {
                var expectedDate = new Date(this.date.getFullYear() + 3, 2, 1);
                var cron = createCronByDate(expectedDate, ['month', 'year']);

                var callCount = 0;
                this.createCronJob(cron, function() { callCount++; });

                this.clock.tick(expectedDate.getTime() - this.date.getTime() - 1 + HOUR);
                expect(callCount).to.be.equal(MINUTES_IN_HOUR);
            });
        });
    });

    describe('ranges', function() {
        var cronJob;

        afterEach(function() {
            cronJob && cronJob.stop();
        });

        it('executes correct times', function() {
            var callCount = 0;

            cronJob = new CronJob('15,20 3-4 * * * *', function() { callCount++; });

            this.clock.tick(DAY);
            expect(callCount).to.be.equal(4);
        });

        it('executes with correct dates', function() {
            var callCount = 0;

            var now = Date.now();
            var nextDay = new Date(now - now % DAY);
            nextDay.setDate(nextDay.getDate() + 1);

            this.clock.tick(nextDay.getTime() - now);

            var expectedTimes = [
                { hour: 10, minute: 3 },
                { hour: 10, minute: 33 },
                { hour: 11, minute: 3 },
                { hour: 11, minute: 33 },
                { hour: 12, minute: 3 },
                { hour: 12, minute: 33 }
            ];

            var expectedDates = expectedTimes.map(function(time) {
                var date = new Date(nextDay);
                date.setHours(time.hour);
                date.setMinutes(time.minute);
                return date;
            });

            cronJob = new CronJob('3,33 10-12 * * * *', function() {
                expect(new Date()).to.be.deep.equal(expectedDates[callCount]);
                callCount++;
            });

            this.clock.tick(DAY);
        });
    });

    describe('steps', function() {
        var cronJob;

        afterEach(function() {
            cronJob && cronJob.stop();
        });

        it('executes correct times', function() {
            var callCount = 0;

            cronJob = new CronJob('*/2 * * * * *', function() { callCount++; });

            this.clock.tick(HOUR);
            expect(callCount).to.be.equal(30);
        });

        it('executed with correct dates', function() {
            var callCount = 0;
            var now = new Date();

            var nextDate = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);
            this.clock.tick(nextDate.getTime() - now.getTime());

            var expectedDates = [
                new Date(nextDate.getFullYear(), nextDate.getMonth(), nextDate.getDate(), 0, 5),
                new Date(nextDate.getFullYear(), nextDate.getMonth(), nextDate.getDate(), 10, 5),
                new Date(nextDate.getFullYear(), nextDate.getMonth(), nextDate.getDate(), 20, 5)
            ];

            cronJob = new CronJob('5 */10 * * * *', function() {
                expect(new Date()).to.be.deep.equal(expectedDates[callCount]);
                callCount++;
            });

            this.clock.tick(DAY);
        });

        it('executes correct time if range is specified', function() {
            var callCount = 0;

            cronJob = new CronJob('1-15/2 * * * * *', function() { callCount ++ });

            this.clock.tick(HOUR);
            expect(callCount).to.be.equal(8);
        });

        it('executes with correct dates if range is specified', function() {
            var callCount = 0;

            var now = new Date();
            var nextDate = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);
            this.clock.tick(nextDate.getTime() - now.getTime());

            var expectedDates = [
                new Date(nextDate.getFullYear(), nextDate.getMonth(), nextDate.getDate(), 2, 13),
                new Date(nextDate.getFullYear(), nextDate.getMonth(), nextDate.getDate(), 5, 13),
                new Date(nextDate.getFullYear(), nextDate.getMonth(), nextDate.getDate(), 8, 13)
            ];

            cronJob = new CronJob('13 2-10/3 * * * *', function() {
                expect(new Date()).to.be.deep.equal(expectedDates[callCount]);
                callCount++;
            });

            this.clock.tick(DAY);
        });
    });

    describe('extended format', function() {
        var cronJob;

        afterEach(function() {
            cronJob && cronJob.stop();
        });

        it('executed correct time if range is specified', function() {
            var callCount = 0;

            cronJob = new CronJob('5', function() {
                callCount++;
            });

            this.clock.tick(DAY);
            expect(callCount).to.be.equal(HOURS_IN_DAY);
        });
    });

    describe('seconds and milliseconds', function() {
        var cronJob;

        afterEach(function() {
            cronJob && cronJob.stop();
        });

        it('executes correct time if second is asterisk', function() {
            var callCount = 0;

            cronJob = new CronJob('* * * * * * *', function() {
                callCount++;
            });

            this.clock.tick(SECOND * 30);
            expect(callCount).to.be.equal(30);
        });

        it('executes with correct dates if second is asterisk', function() {
            this.clock.tick(SECOND * 12 + 531);

            var date = new Date();
            date.setSeconds(date.getSeconds() + 1);
            date.setMilliseconds(0);

            cronJob = new CronJob('* * * * * * *', function() {
                expect(new Date()).to.be.deep.equal(date);
                date.setSeconds(date.getSeconds() + 1);
            });

            this.clock.tick(SECOND * 10);
        });

        it('executes correct time if millisecond is asterisk', function() {
            var callCount = 0;

            cronJob = new CronJob('* * * * * * * *', function() {
                callCount++;
            });

            this.clock.tick(13);
            expect(callCount).to.be.equal(13);
        });

        it('executes with correct dates if millisecond is asterisk', function() {
            this.clock.tick(SECOND * 6 + 899);

            var date = new Date();
            date.setMilliseconds(date.getMilliseconds() + 1);

            cronJob = new CronJob('* * * * * * * *', function() {
                expect(new Date()).to.be.deep.equal(date);
                date.setMilliseconds(date.getMilliseconds() + 1);
            });

            this.clock.tick(8);
        });
    });

    describe('stop()', function() {
        it('stops with "* * * * * *" cron', function() {
            var callCount = 0;

            var cronJob = new CronJob('* * * * * *', function() { callCount++; });
            cronJob.stop();

            this.clock.tick(MINUTE * 20);
            expect(callCount).to.be.equal(0);
        });

        it('stops with "m h d * * *" cron', function() {
            var callCount = 0;
            var nextYearDate = new Date(this.date);
            nextYearDate.setFullYear(nextYearDate.getFullYear());

            var cronJob = new CronJob('4 5 3 * * *', function() { callCount++; });
            cronJob.stop();

            this.clock.tick(nextYearDate.getTime() - this.date.getTime());
            expect(callCount).to.be.equal(0);
        });

        it('stops with "* h * * * *" cron', function() {
            var callCount = 0;

            var cronJob = new CronJob('* 5 * * * *', function() { callCount++ });
            cronJob.stop();

            this.clock.tick(DAY * 20);
            expect(callCount).to.be.equal(0);
        });
    });

    describe('useTimeout option', function() {
        var cronJob;

        beforeEach(function() {
            var originalSetInterval = setInterval;
            var originalSetTimeout = setTimeout;

            this.setIntervalStub = sinon.stub(global, 'setInterval', originalSetInterval);
            this.setTimeoutStub = sinon.stub(global, 'setTimeout', originalSetTimeout);

            this.createIntervalJob = function(interval, func, options) {
                return this.intervalJob = new IntervalJob(interval, func, options);
            };
        });

        afterEach(function() {
            cronJob && cronJob.stop();

            this.setTimeoutStub.restore();
            this.setIntervalStub.restore();
        });

        it('calls setInterval by default', function() {
            cronJob = new CronJob('* * * * * * * *', function() {});
            expect(this.setIntervalStub.callCount).to.be.equal(1);
            expect(this.setTimeoutStub.callCount).to.be.equal(0);
        });

        it('calls setInterval if useTimeout is false', function() {
            cronJob = new CronJob('* * * * * * * *', function() {}, { useTimeout: false });
            expect(this.setIntervalStub.callCount).to.be.equal(1);
            expect(this.setTimeoutStub.callCount).to.be.equal(0);
        });

        it('calls setTimeout if useTimeout is true', function() {
            cronJob = new CronJob('* * * * * * * *', function() {}, { useTimeout: true });

            expect(this.setTimeoutStub.callCount).to.be.equal(1);
            expect(this.setIntervalStub.callCount).to.be.equal(0);

            this.clock.tick(1);

            expect(this.setTimeoutStub.callCount).to.be.equal(2);
            expect(this.setIntervalStub.callCount).to.be.equal(0);
        });
    });
});
