var dateUtils = require('./dateUtils');

var defaultCron = '0 0 * * * * * *';
var maxPartSizes = [ 1000, 60, 60, 24, 31, 12, 7 ];
var mainPartsCount = 6;
var secondaryPartsCount = 2;

function getLeapYearInterval(startDate) {
    var date = new Date(startDate);

    return function() {
        var previousTime = date.getTime();
        var nextYear = date.getFullYear() + 4;

        if(!dateUtils.isYearLeap(nextYear)) {
            nextYear += 4;
        }

        date.setFullYear(nextYear);
        return date.getTime() - previousTime;
    };
}

function getMonthInterval(startDate) {
    var date = new Date(startDate);

    return function() {
        var previousTime = date.getTime();
        date.setMonth(date.getMonth() + 1);
        return date.getTime() - previousTime;
    };
}

function getConditionalMonthInterval(startDate, day) {
    var date = new Date(startDate);

    return function() {
        var previousTime = date.getTime();
        var nextMonth = date.getMonth() + 1;

        if(dateUtils.getDaysCountInMonth(nextMonth + 1, startDate.getFullYear()) < day) {
            nextMonth++;
        }

        date.setMonth(nextMonth);
        return date.getTime() - previousTime;
    };
}

function getYearInterval(startDate) {
    var date = new Date(startDate);

    return function() {
        var previousTime = date.getTime();
        date.setFullYear(date.getFullYear() + 1);
        return date.getTime() - previousTime;
    };
}

function getInterval(cron, startDate) {
    if(cron.cyclicField === cron.fieldIndices.year
        && cron.month.value === 2
        && cron.day.value === 29) {
        return getLeapYearInterval(startDate);
    }

    var fieldName = cron.fields[cron.cyclicField];

    if(fieldName === 'weekday') {
        fieldName = 'week';
    }

    var interval = dateUtils.timePeriods[fieldName];

    if(interval)
        return interval;

    if(cron.cyclicField === cron.fieldIndices.month) {
        return cron.day.value <= 28
            ? getMonthInterval(startDate)
            : getConditionalMonthInterval(startDate, cron.day.value);
    }

    if(cron.cyclicField === cron.fieldIndices.year) {
        return getYearInterval(startDate);
    }
}

function getCyclicTimestamp(cron) {
    var result = 0;

    if(cron.millisecond.asterisk) {
        return result;
    } else {
        result += cron.millisecond.value;
    }

    if(cron.second.asterisk) {
        return result;
    } else {
        result += cron.second.value * dateUtils.timePeriods.second;
    }

    if(cron.minute.asterisk) {
        return result;
    } else {
        result += cron.minute.value * dateUtils.timePeriods.minute;
    }

    if(cron.hour.asterisk) {
        return result;
    } else {
        result += cron.hour.value * dateUtils.timePeriods.hour;
    }

    if(cron.day.asterisk) {
        return result;
    } else {
        result += cron.day.value * dateUtils.timePeriods.day;
    }

    return result;
}

function getPeriod(cron) {
    if(cron.millisecond.asterisk) {
        return 1;
    }

    if(cron.second.asterisk) {
        return dateUtils.timePeriods.second;
    }

    if(cron.minute.asterisk) {
        return dateUtils.timePeriods.minute;
    }

    if(cron.hour.asterisk) {
        return dateUtils.timePeriods.hour;
    }

    if(cron.day.asterisk) {
        return dateUtils.timePeriods.day;
    }
}

function getCyclicStartDate(cron, date) {
    var timezoneOffset = date.getTimezoneOffset();
    var time = date.getTime();
    var utcTime = time - timezoneOffset * dateUtils.timePeriods.minute;

    var expectedTimestamp = getCyclicTimestamp(cron);
    var period = getPeriod(cron);
    var realTimestamp = utcTime % period;

    return new Date(
        time
        - realTimestamp
        + expectedTimestamp
        + (expectedTimestamp > realTimestamp ? 0 : period)
    );
}

function getMonthStartDate(cron, date) {
    date = new Date(date);
    var currentDay = date.getDate();

    date.setMilliseconds(cron.millisecond.value);
    date.setSeconds(cron.second.value);
    date.setMinutes(cron.minute.value);
    date.setHours(cron.hour.value);
    date.setDate(cron.day.value);

    if(currentDay >= cron.day.value) {
        date.setMonth(date.getMonth() + 1);
    }

    return date;
}

function getWeekdayStartDate(cron, date) {
    date = new Date(date);
    var currentWeekday = date.getDay();

    var nextWeekdayDifference = cron.weekday.value < currentWeekday
        ? 7 - currentWeekday + cron.weekday.value
        : cron.weekday.value - currentWeekday;

    date.setDate(date.getDate() + nextWeekdayDifference);
    date.setHours(cron.hour.value);
    date.setMinutes(cron.minute.value);
    date.setSeconds(0);
    date.setMilliseconds(0);

    return date;
}

function getYearStartDate(cron, date) {
    date = new Date(date);
    var currentMonth = date.getMonth() + 1;

    date.setMilliseconds(0);
    date.setSeconds(0);
    date.setMinutes(cron.minute.value);
    date.setHours(cron.hour.value);
    date.setDate(cron.day.value);
    date.setMonth(cron.month.value - 1);

    if(currentMonth >= cron.month.value) {
        date.setFullYear(date.getFullYear() + 1);
    }

    return date;
}

function getDateByCron(cron) {
    return new Date(
        cron.year.value,
        cron.month.value - 1,
        cron.day.value,
        cron.hour.value,
        cron.minute.value
    );
}

function getLeapYearStartDate(cron, date) {
    var nextLeapYear = date.getFullYear();
    nextLeapYear += 4 - nextLeapYear % 4;

    if(!dateUtils.isYearLeap(nextLeapYear)) {
        nextLeapYear += 4;
    }

    return new Date(
        nextLeapYear,
        cron.month.value - 1,
        cron.day.value,
        cron.hour.value,
        cron.minute.value
    );
}

function getNextExecutionDate(cron, date) {
    if(cron.millisecond.asterisk) {
        return null;
    }

    if(cron.year.asterisk
        && cron.month.value === 2
        && cron.day.value === 29) {
        return getLeapYearStartDate(cron, date);
    }

    if(!cron.weekday.asterisk && cron.day.asterisk)
        return getWeekdayStartDate(cron, date);

    if(cron.second.asterisk
        || cron.minute.asterisk
        || cron.hour.asterisk
        || cron.day.asterisk)
        return getCyclicStartDate(cron, date);

    if(cron.month.asterisk)
        return getMonthStartDate(cron, date);

    if(cron.year.asterisk)
        return getYearStartDate(cron, date);

    var executionDate = getDateByCron(cron);

    if(executionDate >= date) {
        return executionDate;
    }

    return null;
}

function shouldCronBeNormalized(cronString) {
    return cronString.indexOf('/') >= 0;
}

function getCommaRangeByStep(step, min, max) {
    var current = min + step;
    var result = min;

    while(current <= max) {
        result += ',' + current;
        current += step;
    }

    return result;
}

function normalizeCronPart(cronPart, index) {
    if(index >= maxPartSizes.length) {
        return cronPart;
    }

    var splittedCronPart = cronPart.split('/');

    if(splittedCronPart.length === 1) {
        return cronPart;
    }

    var min, max;

    if(splittedCronPart[0] === '*') {
        min = 0;
        max = maxPartSizes[index] - 1;
    } else {
        var splittedRange = splittedCronPart[0].split('-');
        min = Number(splittedRange[0]);
        max = Number(splittedRange[1]);
    }

    return getCommaRangeByStep(Number(splittedCronPart[1]), min, max);
}

function normalizeExtendedFormat(cronString) {
    if(!cronString) {
        return defaultCron;
    }

    var splittedCron = cronString.split(' ');
    var splittedCronCount = splittedCron.length;

    if(splittedCronCount === mainPartsCount + secondaryPartsCount) {
        return cronString;
    }

    var i;

    for(i = splittedCronCount; i < mainPartsCount; i++) {
        splittedCron.push('*');
    }

    for(i = Math.max(splittedCronCount - mainPartsCount, 0); i < secondaryPartsCount; i++) {
        splittedCron = [0].concat(splittedCron);
    }

    return splittedCron.join(' ');
}

function normalizeCron(cronString) {
    cronString = normalizeExtendedFormat(cronString);

    if(!shouldCronBeNormalized(cronString)) {
        return cronString;
    }

    return cronString
        .split(' ')
        .map(normalizeCronPart)
        .join(' ');
}

module.exports = {
    getInterval: getInterval,
    getNextExecutionDate: getNextExecutionDate,
    normalizeCron: normalizeCron
};
