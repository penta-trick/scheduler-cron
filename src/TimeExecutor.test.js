var expect = require('chai').expect;
var sinon = require('sinon');

var TimeExecutor = require('./TimeExecutor');

describe('TimeExecutor', function() {
    beforeEach(function() {
        this.clock = sinon.useFakeTimers(Date.now());
    });

    afterEach(function() {
        this.clock.restore();
    });

    it('executed with correct time', function() {
        var date = new Date();
        date.setSeconds(date.getSeconds() + 1);

        new TimeExecutor(date, function() {
            expect(new Date()).to.be.deep.equal(date);
        });

        this.clock.tick(1000);
    });

    it('does not execute function if the cancel method is called', function() {
        var spy = sinon.spy();
        var date = new Date();
        date.setSeconds(date.getSeconds() + 1);

        var timeExecutor = new TimeExecutor(date, spy);
        timeExecutor.cancel();

        this.clock.tick(1000);
        expect(spy.called).to.be.false;
    });
});
