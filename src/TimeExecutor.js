function TimeExecutor(date, func) {
    var timeout = setTimeout(func, date.getTime() - Date.now());

    this.cancel = function() {
        clearTimeout(timeout);
    };
}

module.exports = TimeExecutor;
