var dateUtils = require('./dateUtils');
var cronUtils = require('./cronUtils');

var MILLISECONDS_IN_SECOND = 1000;
var SECONDS_IN_MINUTE = 60;
var MINUTES_IN_HOUR = 60;
var HOURS_IN_DAY = 24;
var MONTHS_IN_YEAR = 12;

var REPEATIONS_COUNT_IN_TIMELINE = [
    MILLISECONDS_IN_SECOND,
    SECONDS_IN_MINUTE,
    MINUTES_IN_HOUR,
    HOURS_IN_DAY
];

var FIELDS = ['millisecond', 'second', 'minute', 'hour', 'day', 'month', 'weekday', 'year'];
var SEQUENT_FIELDS = ['millisecond', 'second', 'minute', 'hour', 'day', 'month', 'year'];

var DEFAULT_VALUES = {
    millisecond: 0,
    second: 0,
    minute: 0,
    hour: 0,
    day: 1,
    month: 1,
    weekday: 1,
    year: 0
};

var FIELD_INDICES = {
    millisecond: 0,
    second: 1,
    minute: 2,
    hour: 3,
    day: 4,
    month: 5,
    weekday: 6,
    year: 7
};

function parseCronFields(cron) {
    var cronArray = cron.split(' ');

    for(var i = 0, n = FIELDS.length; i < n; i++) {
        var cronValue = cronArray[i];
        var item = {};

        item.asterisk = cronValue === '*';

        if(!item.asterisk) {
            item.value = Number(cronValue);
        }

        this[FIELDS[i]] = item;
    }
}

function copyCronMap(cronMap) {
    for(var i = 0, n = FIELDS.length; i < n; i++) {
        this[FIELDS[i]] = cronMap[FIELDS[i]];
    }
}

function convertMapToString(cronMap) {
    var result = [];

    for(var i = 0, n = FIELDS.length; i < n; i++) {
        var item = cronMap[FIELDS[i]];

        if(item.asterisk) {
            result.push('*');
            continue;
        }

        result.push(item.value);
    }

    return result.join(' ');
}

function getSplitIndex() {
    if(!this.weekday.asterisk && this.hour.asterisk) {
        return FIELD_INDICES.weekday;
    }

    if(!this.year.asterisk && this.month.asterisk) {
        return FIELD_INDICES.year;
    }

    var digitsStarted = false;

    for(var i = SEQUENT_FIELDS.length - 2; i >= 0; i--) {
        var key = SEQUENT_FIELDS[i];
        var item = this[key];

        if(!item.asterisk) {
            digitsStarted = true;
        }

        if(digitsStarted && item.asterisk) {
            return i + 1;
        }
    }

    return 0;
}

function cloneObject(object) {
    var result = {};

    for(var key in object) {
        if(object.hasOwnProperty(key)) {
            var value = object[key];
            result[key] = typeof value === 'object' ? cloneObject(value) : value;
        }
    }

    return result;
}

function createMainCronMap(cronMap, splitIndex) {
    var mainCronMap = cloneObject(cronMap);
    var resetCount = splitIndex;

    if(FIELD_INDICES.year === splitIndex) {
        resetCount = FIELD_INDICES.weekday;
    } else if(FIELD_INDICES.weekday === splitIndex) {
        resetCount = FIELD_INDICES.day;
    }

    for(var i = 0; i < resetCount; i++) {
        var key = FIELDS[i];
        var item = mainCronMap[key];

        if(!item.asterisk)
            continue;

        item.asterisk = false;
        item.value = DEFAULT_VALUES[key];
    }

    return mainCronMap;
}

function createInnerCronMap(cronMap, splitIndex) {
    var innerCronMap = cloneObject(cronMap);
    var innerCronResetIndex = splitIndex;
    var asteriskFound = false;

    while(innerCronResetIndex < FIELD_INDICES.weekday) {
        var splitItem = innerCronMap[FIELDS[innerCronResetIndex]];

        if(!splitItem || splitItem.asterisk) {
            asteriskFound = true;
            break;
        }

        splitItem.asterisk = true;
        delete splitItem.value;

        innerCronResetIndex++;
    }

    if(splitIndex !== FIELD_INDICES.year) {
        innerCronMap.weekday.asterisk = true;
        delete innerCronMap.weekday.value;
    }

    if(!asteriskFound) {
        innerCronMap.year.asterisk = true;
        delete innerCronMap.year.value;
    }

    return innerCronMap;
}

function getCyclicStartIndex(cronMap, splitIndex) {
    for(var i = splitIndex; i > 0; i--) {
        if(!cronMap[FIELDS[i - 1]].asterisk) {
            return i;
        }
    }

    return 0;
}

function getMonthDaysCount(month, year) {
    if(month === 2) {
        return (year % 4 > 0) ? 28 : 29;
    }

    if(
        (month <= 7 && month % 2 === 1)
        || (month > 7 && month % 2 === 0)
    ) {
        return 31;
    }

    return 30;
}

function isLeapYear(year) {
    return (year % 4 === 0 && year % 100 !== 0)
        || (year % 400 === 0);
}

function getYearDaysCount(year) {
    return isLeapYear(year) ? 366 : 365;
}

function getMaxExecutionCount(cyclicStartIndex, splitIndex, date) {
    var result = 1;

    if(splitIndex === FIELD_INDICES.year) {
        if(cyclicStartIndex === FIELD_INDICES.month) {
            result *= MONTHS_IN_YEAR;
        } else {
            result *= getYearDaysCount(date.getFullYear());
        }
    }

    var maxMultiplierIndex = Math.min(splitIndex, REPEATIONS_COUNT_IN_TIMELINE.length);

    if(splitIndex === FIELD_INDICES.weekday) {
        maxMultiplierIndex = FIELD_INDICES.day;
    }

    for(var i = cyclicStartIndex; i < maxMultiplierIndex; i++) {
        result *= REPEATIONS_COUNT_IN_TIMELINE[i];
    }

    if(splitIndex === FIELD_INDICES.month) {
        result *= getMonthDaysCount(date.getMonth() + 1, date.getFullYear());
    }

    return result;
}

function getNestedExecutionCount(splitIndex, date) {
    var cyclicStartIndex = getCyclicStartIndex(this, splitIndex);
    return getMaxExecutionCount(cyclicStartIndex, splitIndex, date);
}

function getCyclicIndex() {
    if(this.hasNesting || !this.year.asterisk)
        return;

    var fields = this.fields;
    var fieldIndices = this.fieldIndices;

    if(this.year.asterisk && !this.month.asterisk)
        return fieldIndices.year;

    if(!this.weekday.asterisk && this.day.asterisk) {
        return fieldIndices.weekday;
    }

    for(var i = fieldIndices.month; i > 0; i--) {
        if(this[fields[i - 1]].asterisk)
            continue;

        if(i - 1 === fieldIndices.weekday)
            return fieldIndices.weekday;

        if(i - 1 === fieldIndices.month)
            return fieldIndices.year;

        return i;
    }

    return 0;
}

function isDateCorrect(date) {
    var isDateCorrect = true;

    if(!this.year.asterisk) {
        isDateCorrect = date.getFullYear() === this.year.value;
    }

    if(isDateCorrect && !this.weekday.asterisk) {
        isDateCorrect = date.getDay() === (this.weekday.value % dateUtils.timeSizes.daysInWeek);
    }

    return isDateCorrect;
}

function hasArray(cron) {
    return cron.indexOf(',') >= 0 || cron.indexOf('-') >= 0;
}

function getValues(part) {
    var hyphenParts = part.split('-');

    if(hyphenParts.length > 1) {
        var result = [];

        for(var i = Number(hyphenParts[0]), n = Number(hyphenParts[1]); i <= n; i++) {
            result.push(i);
        }

        return result;
    } else {
        return part.split(',');
    }
}

function convertToArray(cron) {
    var parts = cron.split(' ');
    var crons = getValues(parts[0]);

    for(var i = 1, n = parts.length; i < n; i++) {
        var partArray = getValues(parts[i]);
        var newCrons = [];

        for(var cronIndex = 0, cronsCount = crons.length; cronIndex < cronsCount; cronIndex++) {
            for(var partIndex = 0, valuesCount = partArray.length; partIndex < valuesCount; partIndex++) {
                newCrons.push(crons[cronIndex] + ' ' + partArray[partIndex]);
            }
        }

        crons = newCrons;
    }

    var result = [];

    for(var j = 0, m = crons.length; j < m; j++) {
        result.push(new Cron(crons[j]));
    }

    return result;
}

function Cron(cron) {
    this.fields = FIELDS;
    this.fieldIndices = FIELD_INDICES;

    var cronString;
    var isCronString = typeof cron === 'string';

    if(isCronString) {
        cron = cronUtils.normalizeCron(cron);
        cronString = cron;
    } else {
        copyCronMap.apply(this, [cron]);
        cronString = convertMapToString(cron);
    }

    this.hasArray = hasArray(cronString);

    if(this.hasArray) {
        this.array = convertToArray(cronString);
    } else {
        if(isCronString) {
            parseCronFields.apply(this, [cron]);
        }

        var splitIndex = getSplitIndex.apply(this);
        this.hasNesting = splitIndex > 0;
        this.hasCondition = !(this.year.asterisk && this.weekday.asterisk);

        if(this.hasNesting) {
            this.main = new Cron(createMainCronMap(this, splitIndex));
            this.nested = new Cron(createInnerCronMap(this, splitIndex));
        }

        this.cyclicField = getCyclicIndex.apply(this);

        this.isDateCorrect = isDateCorrect;
        this.getNestedExecutionCount = getNestedExecutionCount.bind(this, splitIndex);
    }

    this.toString = function() {
        return cronString;
    };
}

module.exports = Cron;
