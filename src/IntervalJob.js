var TimeExecutor = require('./TimeExecutor');

var IntervalJob = function(interval, func, options) {
    options = options || {};
    var startDate = options.startDate;
    var useTimeout = options.useTimeout;

    var timeoutInstance;
    var timeExecutor;
    var intervalInstance;
    var executor;
    var executionTime;

    function createIntervalFunctionTimeoutExecutor(intervalFunc, func) {
        executionTime += intervalFunc();

        timeoutInstance = setTimeout(function() {
            func();
            createIntervalFunctionTimeoutExecutor(intervalFunc, func);
        }, executionTime - Date.now());
    }

    function createIntervalTimeoutExecutor(interval, func) {
        executionTime += interval;

        timeoutInstance = setTimeout(function() {
            func();
            createIntervalTimeoutExecutor(interval, func);
        }, executionTime - Date.now());
    }

    if(typeof interval === 'function') {
        executor = function() { createIntervalFunctionTimeoutExecutor(interval, func); };
    } else if(useTimeout) {
        executor = function() { createIntervalTimeoutExecutor(interval, func); };
    } else {
        executor = function() { intervalInstance = setInterval(func, interval); };
    }

    if(startDate) {
        timeExecutor = new TimeExecutor(startDate, function () {
            func();
            executionTime = startDate.getTime();
            executor();
        });
    } else {
        executionTime = Date.now();
        executor();
    }

    this.stop = function() {
        timeExecutor && timeExecutor.cancel();
        timeoutInstance && clearTimeout(timeoutInstance);
        intervalInstance && clearInterval(intervalInstance);
    };
};

module.exports = IntervalJob;
