var expect = require('chai').expect;
var dateUtils = require('./dateUtils');

describe('date utils', function() {
    describe('isYearLeap(year)', function() {
        it('returns true if year is leap', function() {
            expect(dateUtils.isYearLeap(2016)).to.be.true;
        });

        it('returns false if year is not leap', function() {
            expect(dateUtils.isYearLeap(2017)).to.be.false;
        });

        it('returns false if year % 100 === 0', function() {
            expect(dateUtils.isYearLeap(1900)).to.be.false;
        });
    });

    describe('getDaysCountInMonth(month[, year])', function() {
        it('returns correct days count for months with 31 days', function() {
            expect(dateUtils.getDaysCountInMonth(1)).to.be.equal(31);
            expect(dateUtils.getDaysCountInMonth(3)).to.be.equal(31);
            expect(dateUtils.getDaysCountInMonth(5)).to.be.equal(31);
            expect(dateUtils.getDaysCountInMonth(7)).to.be.equal(31);
            expect(dateUtils.getDaysCountInMonth(8)).to.be.equal(31);
            expect(dateUtils.getDaysCountInMonth(10)).to.be.equal(31);
            expect(dateUtils.getDaysCountInMonth(12)).to.be.equal(31);
        });

        it('returns correct days count for months with 30 days', function() {
            expect(dateUtils.getDaysCountInMonth(4)).to.be.equal(30);
            expect(dateUtils.getDaysCountInMonth(6)).to.be.equal(30);
            expect(dateUtils.getDaysCountInMonth(9)).to.be.equal(30);
            expect(dateUtils.getDaysCountInMonth(11)).to.be.equal(30);
        });

        it('returns correct days count for February in leap year', function() {
            expect(dateUtils.getDaysCountInMonth(2, 2016)).to.be.equal(29);
        });

        it('returns correct days count for February in not leap year', function() {
            expect(dateUtils.getDaysCountInMonth(2, 2017)).to.be.equal(28);
        });
    });

    describe('getFirstDayByWeekday(weekday, month, year)', function() {
        it('returns correct date if weekday in the first week', function() {
            expect(dateUtils.getFirstDayByWeekday(5, 11, 2016)).to.be.equal(4);
        });

        it('returns correct date if weekday in the second week', function() {
            expect(dateUtils.getFirstDayByWeekday(3, 10, 2016)).to.be.equal(5);
        });
    });

    describe('timePeriods', function() {
        it('millisecond', function() {
            expect(dateUtils.timePeriods.millisecond).to.be.equal(1);
        });

        it('second', function() {
            expect(dateUtils.timePeriods.second).to.be.equal(1000);
        });

        it('minute', function() {
            expect(dateUtils.timePeriods.minute).to.be.equal(1000 * 60);
        });

        it('hour', function() {
            expect(dateUtils.timePeriods.hour).to.be.equal(1000 * 60 * 60);
        });

        it('day', function() {
            expect(dateUtils.timePeriods.day).to.be.equal(1000 * 60 * 60 * 24);
        });

        it('week', function() {
            expect(dateUtils.timePeriods.week).to.be.equal(1000 * 60 * 60 * 24 * 7);
        });
    });

    describe('timeSizes', function() {
        it('millisecondsInSecond', function() {
            expect(dateUtils.timeSizes.millisecondsInSecond).to.be.equal(1000);
        });

        it('secondsInMinute', function() {
            expect(dateUtils.timeSizes.secondsInMinute).to.be.equal(60);
        });

        it('minutesInHour', function() {
            expect(dateUtils.timeSizes.minutesInHour).to.be.equal(60);
        });

        it('hoursInDay', function() {
            expect(dateUtils.timeSizes.hoursInDay).to.be.equal(24);
        });

        it('daysInWeek', function() {
            expect(dateUtils.timeSizes.daysInWeek).to.be.equal(7);
        });
    });
});
