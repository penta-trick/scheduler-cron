var millisecondsInSecond = 1000;
var secondsInMinute = 60;
var minutesInHour = 60;
var hoursInDay = 24;
var daysInWeek = 7;

var millisecond = 1;
var second = millisecondsInSecond * millisecond;
var minute = secondsInMinute * second;
var hour = minutesInHour * minute;
var day = hoursInDay * hour;
var week = daysInWeek * day;

function isYearLeap(year) {
    return (year % 4 === 0 && year % 100 !== 0)
        || (year % 400 === 0);
}

function getDaysCountInMonth(month, year) {
    if(month === 2) {
        return isYearLeap(year) ? 29 : 28;
    }

    if((month <= 7 && month % 2 === 1)
        || (month > 7 && month % 2 === 0)) {
        return 31;
    }

    return 30;
}

function getFirstDayByWeekday(weekday, month, year) {
    var date = new Date(year, month - 1, 1);
    var firstWeekday = date.getDay();
    var weekdaysDifference = weekday - firstWeekday + 1;

    if(firstWeekday > weekday) {
        weekdaysDifference += daysInWeek;
    }

    return weekdaysDifference;
}

module.exports = {
    isYearLeap: isYearLeap,
    getDaysCountInMonth: getDaysCountInMonth,
    getFirstDayByWeekday: getFirstDayByWeekday,
    timePeriods: {
        millisecond: millisecond,
        second: second,
        minute: minute,
        hour: hour,
        day: day,
        week: week
    },
    timeSizes: {
        millisecondsInSecond: millisecondsInSecond,
        secondsInMinute: secondsInMinute,
        minutesInHour: minutesInHour,
        hoursInDay: hoursInDay,
        daysInWeek: daysInWeek
    }
};
