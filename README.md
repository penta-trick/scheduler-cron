[![Run Status](https://api.shippable.com/projects/57c05933fa8c170f0091b621/badge?branch=master)](https://app.shippable.com/projects/57c05933fa8c170f0091b621)

# Scheduler Cron

It is the compact library for asynchronous function execution. The execution can be in the specified date, the specified static or dynamic interval or complex interval using Cron. The library has no dependencies that is doing it more transparent. Their components are really fast. The performance is improved always. Scheduler Cron will help you to execute some issues without user intervention. It can be server statistics uploading or periodic notifications.

## Real usage

Scheduler Cron is used in [Penta-Trick Fooball Manager](http://penta-trick.com) that helps to find and fix bugs as fast as it possible.

## Components

### [TimeExecutor](https://bitbucket.org/penta-trick/scheduler-cron/wiki/TimeExecutor)

It is designed for execution in specified date. Specify the date and function and it will be executed in this date.

### [IntervalJob](https://bitbucket.org/penta-trick/scheduler-cron/wiki/IntervalJob)

It is designed for execution in specified interval. The interval can be passed statically using numeric value or can be got dynamically by function call.

### [CronJob](https://bitbucket.org/penta-trick/scheduler-cron/wiki/CronJob)

It is designed for execution using Cron. You can use all of [Cron formats](https://bitbucket.org/penta-trick/scheduler-cron/wiki/Cron%20format) which are [supported](https://bitbucket.org/penta-trick/scheduler-cron/wiki/Format%20support).

## Reasons to use

Still not sure whether to use or not? Read [the comparison of Scheduler Cron with JS realization](https://bitbucket.org/penta-trick/scheduler-cron/wiki/Comparison%20of%20Scheduler%20Cron%20and%20JS%20realization).

## Development

Follow the [Trello board](https://trello.com/b/7ic4amOK) to see the development progress and realized features. [Versions history list](https://bitbucket.org/penta-trick/scheduler-cron/wiki/Versions%20history). Did you find a bug or you have a suggestion? Write it [here](https://bitbucket.org/penta-trick/scheduler-cron/issues). Feel free to contact me if you need more information about the library or you have any other question (help@penta-trick.com).
